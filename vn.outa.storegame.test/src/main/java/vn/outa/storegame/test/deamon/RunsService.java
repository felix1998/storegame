package vn.outa.storegame.test.deamon;

import java.util.Date;
import java.util.Random;
import vn.outa.framework.util.StringUtil;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticListResult;
import vn.outa.storegame.thrift.thrift.user.EActive;
import vn.outa.storegame.thrift.thrift.user.EStatusResult;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thrift.user.TUserListResult;
import vn.outa.storegame.thrift.thriftclient.GameClient;
import vn.outa.storegame.thrift.thriftclient.GoldLogClient;
import vn.outa.storegame.thrift.thriftclient.GoldStatisticClient;
import vn.outa.storegame.thrift.thriftclient.UserClient;


/**
 *
 * @author felix
 */
public class RunsService {
    
    public static final String THRIFT_SERVICE = "thrift_service";
    
    private static final UserClient clientUser = new UserClient(THRIFT_SERVICE);

    private static final GameClient clientGame = new GameClient(THRIFT_SERVICE);

    private static final GoldLogClient clientGoldLog = new GoldLogClient(THRIFT_SERVICE);
    
    private static final GoldStatisticClient clientGoldStatistic = new GoldStatisticClient(THRIFT_SERVICE);
    
    public static Random ran = new Random();
    
    public static void main(String[] args) {

        System.out.println("-------------DANH SACH TAT CA USER------------------");
        TUserListResult listAll = clientUser.getList(0, -1);
        if(listAll.getStatus() == EStatusResult.OK){
            for(TUser temp: listAll.getListData()){
                System.out.println("user: " + temp.getUserName() + "  --  "+ temp.getDisplayName() 
                        + " - gold: " + temp.getGold() + " --- active: " 
                        + (temp.active== EActive.ONLINE.getValue() ? EActive.ONLINE.name() : EActive.OFFLINE.name())
                        + "  --  lastLogin: " +  getLongDate(temp.getLastLogin()));
            }
        }
        
        int rs = clientUser.login("nguoidung1", "123");
        System.out.println("login result: " + loginResult(rs));
        
        System.out.println("-------------TOP 5 GOLD USER------------------");
        TUserListResult listTop = clientUser.getTopGold(0, 4);
        if(listTop.getStatus() == EStatusResult.OK){
            for(TUser temp: listTop.getListData()){
                System.out.println("user: " + temp.getUserName() + "  --  "+ temp.getDisplayName() 
                        + " - gold: " + temp.getGold() + " --- active: " 
                        + (temp.active== EActive.ONLINE.getValue() ? EActive.ONLINE.name() : EActive.OFFLINE.name())
                        + "  --  lastLogin: " +  getShortDate(temp.getLastLogin()));
            }
        }
        
        boolean rsLogout = clientUser.logout("nguoidung1");
        System.out.println("logout " + rsLogout);
        
        
        TUser value = new TUser();
        value.setUserName("nguoidung12");
        value.setDisplayName("nguyen van 12");
        value.setPassword("123");
        value.setBirthDay("27/05/2022");
        value.setGender(1);
        value.setRegisDate(String.valueOf(System.currentTimeMillis()));
        value.setPhoneNumber("0968764596");
        value.setAddress("14/3 dang van ngu");
        value.setEmail("nguoidung10@gmail.com");
        boolean rsInsert = clientUser.create(value);
        System.out.println("insert result: " + rsInsert);
        
        
        long start1 = System.currentTimeMillis();
        System.out.println("----------------THONG KE TOTAL GOLD BY ALL DAY CUA USER : nguoidung1--------------");
        TGoldStatisticListResult list = clientGoldStatistic.getListByUser("nguoidung1", 0, -1);
        if(list.getStatus() == vn.outa.storegame.thrift.thrift.goldstatistic.EStatusResult.OK){
            for(TGoldStatistic temp: list.getListData()){
                System.out.println("goldlog " + temp.getId() + ": " + temp.getUserName() + " --- game: " 
                        + temp.getGameName() + " - goldchange: " + temp.getTotalGold()+ "   DATE: " + getShortDate(temp.getDay()));
            }
        }
        long end1 = System.currentTimeMillis();
        System.out.println("time: " + (end1-start1));
        
        long start = System.currentTimeMillis();
        System.out.println("------------------THONG KE TOTAL GOLD BY TODAY CUA USER : nguoidung1--DAY : TODAY------------");
        TGoldStatisticListResult listtodayDay = clientGoldStatistic.getListByUserDay("nguoidung1", System.currentTimeMillis(), 0, -1);
        if(listtodayDay.getStatus() == vn.outa.storegame.thrift.thrift.goldstatistic.EStatusResult.OK){
            for(TGoldStatistic temp: listtodayDay.getListData()){
                System.out.println("goldlog " + temp.getId() + ": " + temp.getUserName() + " --- game: " 
                        + temp.getGameName() + " - goldchange: " + temp.getTotalGold() + "   DATE: " + getShortDate(temp.getDay()));
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end-start));
        
        start = System.currentTimeMillis();
        System.out.println("------------------THONG KE TOTAL GOLD BY GAME CUA USER : nguoidung1--GAME : maubinh---------------");
        TGoldStatisticListResult listUserGame = clientGoldStatistic.getListByUserGame("nguoidung1", "maubinh", 0, -1);
        if(listUserGame.getStatus() == vn.outa.storegame.thrift.thrift.goldstatistic.EStatusResult.OK){
            for(TGoldStatistic temp: listUserGame.getListData()){
                System.out.println("goldlog " + temp.getId() + ": " + temp.getUserName() + " --- game: " 
                        + temp.getGameName() + " - goldchange: " + temp.getTotalGold() + "   DATE: " + getShortDate(temp.getDay()));
            }
        }
        end = System.currentTimeMillis();
        System.out.println("time: " + (end-start));
        
    }
    
    public static String loginResult(int rs){
        String result = "";
        switch (rs){
            case 0:
                result = "Login Thanh cong!";
                break;
            case 1:
                result = "Tai khoan khong ton tai!";
                break;
            case 2:
                result = "Sai mat khau!";
                break;
        }
        return result;
    }
    
    public static String getLongDate(final String date) {
        String result = "";
        if(StringUtil.isNullOrEmpty(date)){
            return result;
        }
        Date value = new Date(Long.valueOf(date));
        result = value.toString();
        return result;
    }
    
    public static String getShortDate(final String date) {
        String result = "";
        if(StringUtil.isNullOrEmpty(date)){
            return result;
        }
        Date value = new Date(Long.valueOf(date));
        result = value.getDate() + "/" + (value.getMonth()+1) + "/" + (value.getYear()+1900);
        return result;
    }
}
