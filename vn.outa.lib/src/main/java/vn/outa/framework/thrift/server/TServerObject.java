package vn.outa.framework.thrift.server;

import org.apache.thrift.server.TServer;

public class TServerObject
{
    public String host;
    public int port;
    public TServer server;
}
