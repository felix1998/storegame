package vn.outa.storegame.worker.daemon;

import org.apache.log4j.Logger;
import vn.outa.framework.gearman.GWorkerManager;
import vn.outa.framework.util.LogUtil;
import vn.outa.storegame.worker.helper.ConfigInfo;

/**
 *
 * @author admin
 */
public class RunService {

    private static final Logger logger = LogUtil.getLogger(RunService.class);

    public static void main(String[] args) {
        GWorkerManager gm = new GWorkerManager();
        gm.start(new String[]{ConfigInfo.GEARMAN_WORKER});

        logger.info("gearman worker start!");
    }
}