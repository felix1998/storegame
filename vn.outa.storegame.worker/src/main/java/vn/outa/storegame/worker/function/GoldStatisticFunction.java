package vn.outa.storegame.worker.function;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJobResult;
import org.gearman.client.GearmanJobResultImpl;
import org.gearman.util.ByteUtils;
import org.gearman.worker.AbstractGearmanFunction;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.EncryptUtil;
import vn.outa.framework.util.JsonUtil;
import vn.outa.storegame.db.model.GoldStatisticDA;
import vn.outa.storegame.thrift.thrift.goldstatistic.EJobType;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;

/**
 *
 * @author felix
 */
public class GoldStatisticFunction extends AbstractGearmanFunction {
    
    private static final Logger logger = Logger.getLogger(GoldStatisticFunction.class);

    @Override
    public GearmanJobResult executeFunction() {
        String returnMsg = "";
        boolean result = false;
        try {
            String jobData = EncryptUtil.decodeUTF8((byte[]) this.data);
            Type type = new TypeToken<JobEnt<EJobType>>() {
            }.getType();
            
            JobEnt<EJobType> job = (JobEnt) JsonUtil.deserialize(jobData, type);
            if (job.data != null) {
                TGoldStatistic value = new TGoldStatistic();
                switch (job.type) {
                    case CREATE:
                        value = JsonUtil.deserialize(job.data, TGoldStatistic.class);
                        result = GoldStatisticDA.getInstance().insert(value);
                        break;
                    case UPDATE_GOLD:
                        value = JsonUtil.deserialize(job.data, TGoldStatistic.class);
                        result = GoldStatisticDA.getInstance().updateGold(value);
                        break;
                    case UPDATE_STATUS:
                        value = JsonUtil.deserialize(job.data, TGoldStatistic.class);
                        result = GoldStatisticDA.getInstance().updateStatus(value);
                        break;
                    case DELETE:
                        result = GoldStatisticDA.getInstance().remove(Long.parseLong(job.data));
                        break;
                    default:
                        returnMsg = "Invalid job action : " + data;
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            returnMsg = e.getMessage();
        }

        // log error in case of failures
        if (!result) {
            logger.error(returnMsg);
        }
        // return gearman job result
        byte[] returnData = result ? (byte[]) this.data : new byte[0];
        return new GearmanJobResultImpl(this.jobHandle, result, new byte[0], returnData, ByteUtils.toUTF8Bytes(returnMsg), 0, 0);
    }
    
}
