package vn.outa.storegame.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.storegame.db.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogs;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsListResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsResult;

/**
 *
 * @author felix
 */
public class BaccaratPayoutControlLogsDA {
    private static final Logger logger = Logger.getLogger(BaccaratPayoutControlLogsDA.class);
    
    private static final BaccaratPayoutControlLogsDA instance = new BaccaratPayoutControlLogsDA();
    
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `game_id`, `round_id`, `rtp_config`, `rtp_start`, `total_pay_in_start`, `total_pay_out_start`, `rtp_end`, "
            + "`total_pay_in_end`, `total_pay_out_end`, `round_pay_in`, `round_pay_out`, `pay_in_player_pair`, `pay_in_player`, `pay_in_banker`, `pay_in_tie`, `pay_in_banker_pair`, "
            + "`pay_out_player_pair`, `pay_out_player`, `pay_out_banker`, `pay_out_tie`, `pay_out_banker_pair`, `original_result`, `random_results`, `result`, `is_active`, `time`) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    private static final String UPDATE_QUERY = "UPDATE `%s` SET `game_id` = ?, `round_id` = ?, `rtp_config` = ?, `rtp_start` = ?, `total_pay_in_start` = ?, `total_pay_out_start` = ?, "
            + "`rtp_end` = ?, `total_pay_in_end` = ?, `total_pay_out_end` = ?, `round_pay_in` = ?, `round_pay_out` = ?, `pay_in_player_pair` = ?, `pay_in_player` = ?, "
            + "`pay_in_banker` = ?, `pay_in_tie` = ?, `pay_in_banker_pair` = ?, `pay_out_player_pair` = ?, `pay_out_player` = ?, `pay_out_banker` = ?, `pay_out_tie` = ?, "
            + "`pay_out_banker_pair` = ?, `original_result` = ?, `random_results` = ?, `result` = ?, `is_active` = ?, `time` = ? WHERE `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";
    
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    
    private static final String SELECT_QUERY = "SELECT SQL_CALC_FOUND_ROWS * FROM `%s`";
    
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ?;";
    
    private static final String GET_ALL_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    
    
    public static BaccaratPayoutControlLogsDA getInstance () {
        return instance;
    }
    
    private static TBaccaratPayoutControlLogs createFromReader (ResultSet rs) throws  SQLException {
        TBaccaratPayoutControlLogs item = new TBaccaratPayoutControlLogs();
        item.setId(rs.getLong("id"));
        item.setGameId(rs.getInt("game_id"));
        item.setRoundId(rs.getLong("round_id"));
        item.setRtpConfig(rs.getDouble("rtp_config"));
        item.setRtpStart(rs.getDouble("rtp_start"));
        item.setTotalPayInStart(rs.getLong("total_pay_in_start"));
        item.setTotalPayOutStart(rs.getLong("total_pay_out_start"));
        item.setRtpEnd(rs.getDouble("rtp_end"));
        item.setTotalPayInEnd(rs.getLong("total_pay_in_end"));
        item.setTotalPayOutEnd(rs.getLong("total_pay_out_end"));
        item.setRoundPayIn(rs.getLong("round_pay_in"));
        item.setRoundPayOut(rs.getLong("round_pay_out"));
        item.setPayInPlayerPair(rs.getLong("pay_in_player_pair"));
        item.setPayInPlayer(rs.getLong("pay_in_player"));
        item.setPayInBanker(rs.getLong("pay_in_banker"));
        item.setPayInTie(rs.getLong("pay_in_tie"));
        item.setPayInBankerPair(rs.getLong("pay_in_banker_pair"));
        item.setPayOutPlayerPair(rs.getLong("pay_out_player_pair"));
        item.setPayOutPlayer(rs.getLong("pay_out_player"));
        item.setPayOutBanker(rs.getLong("pay_out_banker"));
        item.setPayOutTie(rs.getLong("pay_out_tie"));
        item.setPayOutBankerPair(rs.getLong("pay_out_banker_pair"));
        item.setOriginalResult(rs.getString("original_result"));
        item.setRandomResults(rs.getString("random_results"));
        item.setResult(rs.getString("result"));
        item.setId(rs.getInt("is_active"));
        item.setId(rs.getLong("time"));
        
        return item;
    }
    
    public long getLastID (){
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        
        String query = String.format(GET_LASTID, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS);
        try(PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        
        return result;
    }
    
    public boolean insert(TBaccaratPayoutControlLogs value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setInt(2, value.getGameId());
            stmt.setLong(3, value.getRoundId());
            stmt.setDouble(4, value.getRtpConfig());
            stmt.setDouble(5, value.getRtpStart());
            stmt.setLong(6, value.getTotalPayInStart());
            stmt.setLong(7, value.getTotalPayOutStart());
            stmt.setDouble(8, value.getRtpEnd());
            stmt.setLong(9, value.getTotalPayInEnd());
            stmt.setLong(10, value.getTotalPayOutEnd());
            stmt.setLong(11, value.getRoundPayIn());
            stmt.setLong(12, value.getRoundPayOut());
            stmt.setLong(13, value.getPayInPlayerPair());
            stmt.setLong(14, value.getPayInPlayer());
            stmt.setLong(15, value.getPayInBanker());
            stmt.setLong(16, value.getPayInTie());
            stmt.setLong(17, value.getPayInBankerPair());
            stmt.setLong(18, value.getPayOutPlayerPair());
            stmt.setLong(19, value.getPayOutPlayer());
            stmt.setLong(20, value.getPayOutBanker());
            stmt.setLong(21, value.getPayOutTie());
            stmt.setLong(22, value.getPayOutBankerPair());
            stmt.setString(23, value.getOriginalResult());
            stmt.setString(24, value.getRandomResults());
            stmt.setString(25, value.getResult());
            stmt.setInt(26, value.getIsActive());
            stmt.setLong(27, value.getTime());
            
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }
    
    public boolean update(TBaccaratPayoutControlLogs value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_QUERY, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1, value.getGameId());
            stmt.setLong(2, value.getRoundId());
            stmt.setDouble(3, value.getRtpConfig());
            stmt.setDouble(4, value.getRtpStart());
            stmt.setLong(5, value.getTotalPayInStart());
            stmt.setLong(6, value.getTotalPayOutStart());
            stmt.setDouble(7, value.getRtpEnd());
            stmt.setLong(8, value.getTotalPayInEnd());
            stmt.setLong(9, value.getTotalPayOutEnd());
            stmt.setLong(10, value.getRoundPayIn());
            stmt.setLong(11, value.getRoundPayOut());
            stmt.setLong(12, value.getPayInPlayerPair());
            stmt.setLong(13, value.getPayInPlayer());
            stmt.setLong(14, value.getPayInBanker());
            stmt.setLong(15, value.getPayInTie());
            stmt.setLong(16, value.getPayInBankerPair());
            stmt.setLong(17, value.getPayOutPlayerPair());
            stmt.setLong(18, value.getPayOutPlayer());
            stmt.setLong(19, value.getPayOutBanker());
            stmt.setLong(20, value.getPayOutTie());
            stmt.setLong(21, value.getPayOutBankerPair());
            stmt.setString(22, value.getOriginalResult());
            stmt.setString(23, value.getRandomResults());
            stmt.setString(24, value.getResult());
            stmt.setInt(25, value.getIsActive());
            stmt.setLong(26, value.getTime());
            stmt.setLong(27, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean remove(long id) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS);
        try (PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, id);
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TBaccaratPayoutControlLogsResult getById(long id) {
        TBaccaratPayoutControlLogsResult result = new TBaccaratPayoutControlLogsResult();
        TBaccaratPayoutControlLogs value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TBaccaratPayoutControlLogsListResult getListMulti(String whereClause) {
        TBaccaratPayoutControlLogsListResult result = new TBaccaratPayoutControlLogsListResult();
        List<TBaccaratPayoutControlLogs> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_ALL_LIST_QUERY, ConfigInfo.TABLE_BACCARAT_PAYOUT_CONTROL_LOGS, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TBaccaratPayoutControlLogs item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
}
