package vn.outa.storegame.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.storegame.db.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogs;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsListResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsResult;

/**
 *
 * @author felix
 */
public class CheatTienLenLogsDA {
    private static final Logger logger = Logger.getLogger(CheatTienLenLogsDA.class);
    
    private static final CheatTienLenLogsDA instance = new CheatTienLenLogsDA();
    
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `min_money_bot`, `min_money_bot_antrang`, `min_total_money_bot`, `game_config`, `bots`, `round_id`, `cheat_bot_id`, `cheat_mode`, `win_lost`, `end_round_game_config`, `total_win_lost_before`, `total_win_lost_after`, `time`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    private static final String UPDATE_QUERY = "UPDATE `%s` SET `min_money_bot` = ?, `min_money_bot_antrang` = ?, `min_total_money_bot` = ?, `game_config` = ?, `bots` = ?, `round_id` = ?, `cheat_bot_id` = ?, `cheat_mode` = ?, `win_lost` = ?, `end_round_game_config` = ?, `total_win_lost_before` = ?, `total_win_lost_after` = ?, `time` = ? WHERE `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`=?;";
    
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    
    private static final String SELECT_QUERY = "SELECT SQL_CALC_FOUND_ROWS * FROM `%s`";
    
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ? ;";
    
    private static final String GET_ALL_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    
    
    public static CheatTienLenLogsDA getInstance () {
        return instance;
    }
    
    private static TCheatTienLenLogs createFromReader (ResultSet rs) throws  SQLException {
        TCheatTienLenLogs item = new TCheatTienLenLogs();
        item.setId(rs.getLong("id"));
        item.setMinMoneyBot(rs.getDouble("min_money_bot"));
        item.setMinMoneyBotAntrang(rs.getDouble("min_money_bot_antrang"));
        item.setMinTotalMoneyBot(rs.getDouble("min_total_money_bot"));
        item.setGameConfig(rs.getString("game_config"));
        item.setBots(rs.getString("bots"));
        item.setRoundId(rs.getLong("round_id"));
        item.setCheatBotId(rs.getString("cheat_bot_id"));
        item.setCheatMode(rs.getString("cheat_mode"));
        item.setWinLost(rs.getDouble("win_lost"));
        item.setEndRoundGameConfig(rs.getString("end_round_game_config"));
        item.setTotalWinLostBefore(rs.getDouble("total_win_lost_before"));
        item.setTotalWinLostAfter(rs.getDouble("total_win_lost_after"));
        item.setTime(rs.getLong("time"));
        
        return item;
    }
    
    public long getLastID (){
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        
        String query = String.format(GET_LASTID, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS);
        try(PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        
        return result;
    }
    
    public boolean insert(TCheatTienLenLogs value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setDouble(2, value.getMinMoneyBot());
            stmt.setDouble(3, value.getMinMoneyBotAntrang());
            stmt.setDouble(4, value.getMinTotalMoneyBot());
            stmt.setString(5, value.getGameConfig());
            stmt.setString(6, value.getBots());
            stmt.setLong(7, value.getRoundId());
            stmt.setString(8, value.getCheatBotId());
            stmt.setString(9, value.getCheatMode());
            stmt.setDouble(10, value.getWinLost());
            stmt.setString(11, value.getEndRoundGameConfig());
            stmt.setDouble(12, value.getTotalWinLostBefore());
            stmt.setDouble(13, value.getTotalWinLostAfter());
            stmt.setLong(14, value.getTime());
            
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }
    
    public boolean update(TCheatTienLenLogs value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_QUERY, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setDouble(1, value.getMinMoneyBot());
            stmt.setDouble(2, value.getMinMoneyBotAntrang());
            stmt.setDouble(3, value.getMinTotalMoneyBot());
            stmt.setString(4, value.getGameConfig());
            stmt.setString(5, value.getBots());
            stmt.setLong(6, value.getRoundId());
            stmt.setString(7, value.getCheatBotId());
            stmt.setString(8, value.getCheatMode());
            stmt.setDouble(9, value.getWinLost());
            stmt.setString(10, value.getEndRoundGameConfig());
            stmt.setDouble(11, value.getTotalWinLostBefore());
            stmt.setDouble(12, value.getTotalWinLostAfter());
            stmt.setLong(13, value.getTime());
            stmt.setLong(14, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean remove(long id) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS);
        try (PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, id);
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TCheatTienLenLogsResult getById(long id) {
        TCheatTienLenLogsResult result = new TCheatTienLenLogsResult();
        TCheatTienLenLogs value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TCheatTienLenLogsListResult getListMulti(String whereClause) {
        TCheatTienLenLogsListResult result = new TCheatTienLenLogsListResult();
        List<TCheatTienLenLogs> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_ALL_LIST_QUERY, ConfigInfo.TABLE_CHEAT_TIENLEN_LOGS, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TCheatTienLenLogs item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
}
