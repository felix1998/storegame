package vn.outa.storegame.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.storegame.db.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.game.EStatus;
import vn.outa.storegame.thrift.thrift.game.EStatusResult;
import vn.outa.storegame.thrift.thrift.game.TGame;
import vn.outa.storegame.thrift.thrift.game.TGameListResult;
import vn.outa.storegame.thrift.thrift.game.TGameResult;

/**
 *
 * @author felix
 */
public class GameDA {
    private static final Logger logger = Logger.getLogger(GameDA.class);
    
    private static final GameDA instance = new GameDA();
    
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `game_name`, `display_name`, `description`, `status`, `start_date`, `end_date`, `created_at`) VALUES (?,?,?,?,?,?,?,NOW());";

    private static final String UPDATE_INFOR_QUERY = "UPDATE `%s` SET `display_name` = ?, `description` = ?, `start_date` = ?, `end_date` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String UPDATE_STATUS_QUERY = "UPDATE `%s` SET `status` = ?,`modified_at` = NOW() WHERE `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";
    
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    
    private static final String SELECT_QUERY = "SELECT SQL_CALC_FOUND_ROWS * FROM `%s`";
    
    private static final String STATUS_VALUE = "AND `status` = %s;";
    
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ? " + STATUS_VALUE;
    
    private static final String GET_BY_ID_NON_QUERY = SELECT_QUERY + " WHERE `id`= ? ;";
    
    private static final String GET_ALL_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    
    private static final String GET_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s " + STATUS_VALUE;;
    
    private static final String GET_BY_GAME_QUERY = SELECT_QUERY + " WHERE `game_name`= ? "+ STATUS_VALUE;
    
    private static final String CHECK_EXISTS_QUERY = SELECT_QUERY + " WHERE `game_name`= ?;";
    
    
    public static GameDA getInstance () {
        return instance;
    }
    
    private static TGame createFromReader (ResultSet rs) throws  SQLException {
        TGame item = new TGame();
        item.setId(rs.getLong("id"));
        item.setGameName(rs.getString("game_name"));
        item.setDisplayName(rs.getString("display_name"));
        item.setDecription(rs.getString("description"));
        item.setStatus(rs.getInt("status"));
        item.setStartDate(rs.getString("start_date"));
        item.setEndDate(rs.getString("end_date"));
        
        return item;
    }
    
    public long getLastID (){
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        
        String query = String.format(GET_LASTID, ConfigInfo.TABLE_GAME);
        try(PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean insert(TGame value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getGameName());
            stmt.setString(3, value.getDisplayName());
            stmt.setString(4, value.getDecription());
            stmt.setInt(5, value.getStatus());
            stmt.setString(6, value.getStartDate());
            stmt.setString(7, value.getEndDate());
            
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }
    
    public boolean updateInformation(TGame value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_INFOR_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, value.getDisplayName());
            stmt.setString(2, value.getDecription());
            stmt.setString(3, value.getStartDate());
            stmt.setString(4, value.getEndDate());
            stmt.setLong(5, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateStatus(TGame value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_STATUS_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1,value.getStatus());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean remove(long id) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, id);
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean checkExists(String gameName) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(CHECK_EXISTS_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, gameName);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result  = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameResult getById(long id) {
        TGameResult result = new TGameResult();
        TGame value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_GAME, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameResult getByIdNon(long id) {
        TGameResult result = new TGameResult();
        TGame value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_NON_QUERY, ConfigInfo.TABLE_GAME);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameResult getByName(String keyword) {
        TGameResult result = new TGameResult();
        TGame value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_GAME_QUERY, ConfigInfo.TABLE_GAME, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, keyword);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            
            result.setValue(value);
            result.setStatus(value == null ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameListResult getList(String whereClause) {
        TGameListResult result = new TGameListResult();
        List<TGame> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_GAME, whereClause, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGame item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameListResult getListMulti(String whereClause) {
        TGameListResult result = new TGameListResult();
        List<TGame> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_ALL_LIST_QUERY, ConfigInfo.TABLE_GAME, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGame item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGameListResult getListDisable(String whereClause) {
        TGameListResult result = new TGameListResult();
        List<TGame> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_GAME, whereClause, EStatus.DISABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGame item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
}
