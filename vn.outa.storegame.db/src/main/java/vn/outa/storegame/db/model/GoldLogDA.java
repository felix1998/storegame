package vn.outa.storegame.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.storegame.db.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.goldLog.EStatus;
import vn.outa.storegame.thrift.thrift.goldLog.EStatusResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLog;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogListResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogResult;

/**
 *
 * @author felix
 */
public class GoldLogDA {
    private static final Logger logger = Logger.getLogger(GoldLogDA.class);
    
    private static final GoldLogDA instance = new GoldLogDA();
    
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `user_name`,`game_name`,`gold_change`,`date`, `status`, `created_at`) VALUES (?,?,?,?,?,?,NOW());";

    private static final String UPDATE_GOLD_QUERY = "UPDATE `%s` SET `gold_change` = ?, `modified_at` = NOW() WHERE `id` = ?;";

    private static final String UPDATE_STATUS_QUERY = "UPDATE `%s` SET `status` = ?, `modified_at` = NOW() WHERE `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`=?;";
    
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    
    private static final String SELECT_QUERY = "SELECT SQL_CALC_FOUND_ROWS * FROM `%s`";
    
    private static final String STATUS_VALUE = "AND `status` = %s;";
    
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ? " + STATUS_VALUE;
    
    private static final String GET_BY_ID_NON_QUERY = SELECT_QUERY + " WHERE `id`= ? ;";
    
    private static final String GET_ALL_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    
    private static final String GET_DISABLE_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s " + STATUS_VALUE;
    
    private static final String GET_BY_USER_QUERY = SELECT_QUERY + " WHERE `user_name`= ? " + STATUS_VALUE;
    
    private static final String GET_BY_USER_GAME_QUERY = SELECT_QUERY + " WHERE `user_name`= ? AND `game_name`= ? " + STATUS_VALUE;
    
    private static final String GET_BY_USER_GAME_DAY_QUERY = SELECT_QUERY + " WHERE `user_name`= ? AND `game_name`= ? AND `date` > ? AND `date` < ? " + STATUS_VALUE;
    
    
    public static GoldLogDA getInstance () {
        return instance;
    }
    
    private static TGoldLog createFromReader (ResultSet rs) throws  SQLException {
        TGoldLog item = new TGoldLog();
        item.setId(rs.getLong("id"));
        item.setUserName(rs.getString("user_name"));
        item.setGameName(rs.getString("game_name"));
        item.setGoldChange(rs.getDouble("gold_change"));
        item.setDate(rs.getLong("date"));
        item.setStatus(rs.getInt("status"));
        
        return item;
    }
    
    public long getLastID (){
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        
        String query = String.format(GET_LASTID, ConfigInfo.TABLE_GOLD_LOG);
        try(PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        
        return result;
    }
    
    public boolean insert(TGoldLog value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_GOLD_LOG);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getUserName());
            stmt.setString(3, value.getGameName());
            stmt.setDouble(4, value.getGoldChange());
            stmt.setLong(5, value.getDate());
            stmt.setInt(6, value.getStatus());

            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }
    
    public boolean updateGold(TGoldLog value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_GOLD_QUERY, ConfigInfo.TABLE_GOLD_LOG);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setDouble(1, value.getGoldChange());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateStatus(TGoldLog value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_STATUS_QUERY, ConfigInfo.TABLE_GOLD_LOG);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1,value.getStatus());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean remove(long id) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_GOLD_LOG);
        try (PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, id);
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogResult getById(long id) {
        TGoldLogResult result = new TGoldLogResult();
        TGoldLog value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_GOLD_LOG, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogResult getByIdNon(long id) {
        TGoldLogResult result = new TGoldLogResult();
        TGoldLog value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_NON_QUERY, ConfigInfo.TABLE_GOLD_LOG);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogListResult getListByUser(String userName) {
        TGoldLogListResult result = new TGoldLogListResult();
        List<TGoldLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_QUERY, ConfigInfo.TABLE_GOLD_LOG, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGoldLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogListResult getListByUserGame(String userName, String gameName) {
        TGoldLogListResult result = new TGoldLogListResult();
        List<TGoldLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_GAME_QUERY, ConfigInfo.TABLE_GOLD_LOG, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            stmt.setString(2, gameName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGoldLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogListResult getListByUserGameDay(String userName, String gameName, long beginDay, long endDay) {
        TGoldLogListResult result = new TGoldLogListResult();
        List<TGoldLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_GAME_DAY_QUERY, ConfigInfo.TABLE_GOLD_LOG, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            stmt.setString(2, gameName);
            stmt.setLong(3, beginDay);
            stmt.setLong(4, endDay);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGoldLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogListResult getListMulti(String whereClause) {
        TGoldLogListResult result = new TGoldLogListResult();
        List<TGoldLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_ALL_LIST_QUERY, ConfigInfo.TABLE_GOLD_LOG, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGoldLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TGoldLogListResult getListDisable(String whereClause) {
        TGoldLogListResult result = new TGoldLogListResult();
        List<TGoldLog> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_DISABLE_LIST_QUERY, ConfigInfo.TABLE_GOLD_LOG, whereClause, EStatus.DISABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TGoldLog item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
}
