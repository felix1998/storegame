package vn.outa.storegame.db.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import vn.outa.framework.db.ConnectionIF;
import vn.outa.framework.db.ConnectionManager;
import vn.outa.storegame.db.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.user.EStatus;
import vn.outa.storegame.thrift.thrift.user.EStatusResult;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thrift.user.TUserListResult;
import vn.outa.storegame.thrift.thrift.user.TUserResult;

/**
 *
 * @author felix
 */
public class UserDA {
    
    private static final Logger logger = Logger.getLogger(UserDA.class);
    
    private static final UserDA instance = new UserDA();
    
    private static final String INSERT_QUERY = "INSERT INTO `%s` (`id`, `user_name`, `password`, `display_name`, `phone_number`, "
            + "`money`, `gold`, `level`, `experence`, `win`, `lose`, `quit`, `active`, `last_login`, `gender`, `birthday`, `email`, "
            + "`address`, `status`, `regis_date`, `created_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW());";

    private static final String UPDATE_INFOR_QUERY = "UPDATE `%s` SET `display_name` = ?, `gender` = ?, `email` = ?, "
            + "`address` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String UPDATE_MONEY_QUERY = "UPDATE `%s` SET `money` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String UPDATE_GOLD_QUERY = "UPDATE `%s` SET `gold` = ?, `modified_at` = NOW() WHERE `id` = ?;";

    private static final String UPDATE_STATUS_QUERY = "UPDATE `%s` SET `status` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String UPDATE_ACTIVE_QUERY = "UPDATE `%s` SET `active` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String UPDATE_PASSWORD_QUERY = "UPDATE `%s` SET `password` = ?, `modified_at` = NOW() WHERE `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `%s` WHERE `id`= ?;";
    
    private static final String GET_LASTID = "SELECT Max(`id`) as LastID FROM `%s`;";
    
    private static final String SELECT_QUERY = "SELECT SQL_CALC_FOUND_ROWS * FROM `%s`";
    
    private static final String STATUS_VALUE = "AND `status` = %s;";    
    
    private static final String GET_BY_ID_QUERY = SELECT_QUERY + " WHERE `id`= ? " + STATUS_VALUE;
    
    private static final String GET_BY_ID_NON_QUERY = SELECT_QUERY + " WHERE `id`= ? ";
    
    private static final String GET_ALL_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s ";
    
    private static final String GET_LIST_QUERY = SELECT_QUERY + " WHERE 1=1 %s " + STATUS_VALUE;
    
    private static final String GET_BY_USER_NAME_QUERY = SELECT_QUERY + " WHERE `user_name`= ? "+ STATUS_VALUE;
    
    private static final String GET_BY_USER_PHONE_QUERY = SELECT_QUERY + " WHERE ( `user_name`= ? or `phone_number` = ? ) "+ STATUS_VALUE;
    
    private static final String LOGIN_QUERY = SELECT_QUERY + " WHERE `user_name`= ? AND `password` = ? " + STATUS_VALUE;
    
    private static final String LOGOUT_QUERY = "UPDATE `%s` SET `active` = ?, `last_login` = ?, `modified_at` = NOW() WHERE `id` = ?;";
    
    private static final String CHECK_EXISTS_QUERY = SELECT_QUERY + " WHERE `user_name`= ? or `phone_number` = ?;";
    

    public static UserDA getInstance () {
        return instance;
    }
    
    private static TUser createFromReader (ResultSet rs) throws  SQLException {
        TUser item = new TUser();
        item.setId(rs.getLong("id"));
        item.setUserName(rs.getString("user_name"));
        item.setPassword(rs.getString("password"));
        item.setDisplayName(rs.getString("display_name"));
        item.setPhoneNumber(rs.getString("phone_number"));
        item.setMoney(rs.getDouble("money"));
        item.setGold(rs.getDouble("gold"));
        item.setLevel(rs.getInt("level"));
        item.setExperence(rs.getDouble("experence"));
        item.setWin(rs.getInt("win"));
        item.setLose(rs.getInt("lose"));
        item.setQuit(rs.getInt("quit"));
        item.setActive(rs.getInt("active"));
        item.setLastLogin(rs.getString("last_login"));
        item.setGender(rs.getInt("gender"));
        item.setBirthDay(rs.getString("birthday"));
        item.setEmail(rs.getString("email"));
        item.setAddress(rs.getString("address"));
        item.setStatus(rs.getInt("status"));
        item.setRegisDate(rs.getString("regis_date"));
        
        return item;
    }
    
    public long getLastID (){
        long result = 0;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        
        String query = String.format(GET_LASTID, ConfigInfo.TABLE_USER);
        try(PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result = rs.getLong("LastID");
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean insert(TUser value) {
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(INSERT_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setLong(1, value.getId());
            stmt.setString(2, value.getUserName());
            stmt.setString(3, value.getPassword());
            stmt.setString(4, value.getDisplayName());
            stmt.setString(5, value.getPhoneNumber());
            stmt.setDouble(6, value.getMoney());
            stmt.setDouble(7, value.getGold());
            stmt.setInt(8, value.getLevel());
            stmt.setDouble(9, value.getExperence());
            stmt.setInt(10, value.getWin());
            stmt.setInt(11, value.getLose());
            stmt.setInt(12, value.getQuit());
            stmt.setInt(13, value.getActive());
            stmt.setString(14, value.getLastLogin());
            stmt.setInt(15, value.getGender());
            stmt.setString(16, value.getBirthDay());
            stmt.setString(17, value.getEmail());
            stmt.setString(18, value.getAddress());
            stmt.setInt(19, value.getStatus());
            stmt.setString(20, value.getRegisDate());
            
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return false;
    }
    
    public boolean updateInformation(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_INFOR_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, value.getDisplayName());
            stmt.setInt(2, value.getGender());
            stmt.setString(3, value.getEmail());
            stmt.setString(4, value.getAddress());
            stmt.setLong(5, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateMoney(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_MONEY_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setDouble(1, value.getMoney());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateGold(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_GOLD_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setDouble(1, value.getGold());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateStatus(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_STATUS_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1,value.getStatus());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updateActive(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_ACTIVE_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1,value.getActive());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean updatePassword(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(UPDATE_PASSWORD_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1,value.getPassword());
            stmt.setLong(2, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean remove(long id) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(DELETE_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement st = cnn.prepareStatement(query, Statement.NO_GENERATED_KEYS)) {
            st.setLong(1, id);
            result = st.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean login(String userName, String password) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(LOGIN_QUERY, ConfigInfo.TABLE_USER, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result  = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean logout(TUser value) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(LOGOUT_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1,value.getActive());
            stmt.setString(2,value.getLastLogin());
            stmt.setLong(3, value.getId());
            result = stmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public boolean checkExists(String userName, String phoneNumber) {
        boolean result = false;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();

        String query = String.format(CHECK_EXISTS_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            stmt.setString(2, phoneNumber);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                result  = true;
            }
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserResult getById(long id) {
        TUserResult result = new TUserResult();
        TUser value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_QUERY, ConfigInfo.TABLE_USER, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserResult getByIdNon(long id) {
        TUserResult result = new TUserResult();
        TUser value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_ID_NON_QUERY, ConfigInfo.TABLE_USER);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserResult getByUserName(String userName) {
        TUserResult result = new TUserResult();
        TUser value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_NAME_QUERY, ConfigInfo.TABLE_USER, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, userName);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserResult getByUserOrPhone(String keyword) {
        TUserResult result = new TUserResult();
        TUser value = null;
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_BY_USER_PHONE_QUERY, ConfigInfo.TABLE_USER, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            stmt.setString(1, keyword);
            stmt.setString(2, keyword);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                value = createFromReader(rs);
            }
            result.setValue(value);
            result.setStatus(value == null? EStatusResult.FAIL : EStatusResult.OK);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserListResult getList(String whereClause) {
        TUserListResult result = new TUserListResult();
        List<TUser> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_USER, whereClause, EStatus.ENABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TUser item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserListResult getListMulti(String whereClause) {
        TUserListResult result = new TUserListResult();
        List<TUser> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_ALL_LIST_QUERY, ConfigInfo.TABLE_USER, whereClause);
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TUser item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserListResult getListDisable(String whereClause) {
        TUserListResult result = new TUserListResult();
        List<TUser> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_USER, whereClause, EStatus.DISABLE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TUser item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
    public TUserListResult getListDelete(String whereClause) {
        TUserListResult result = new TUserListResult();
        List<TUser> lstValue = new ArrayList<>();
        ConnectionIF cm = ConnectionManager.getInstance(ConfigInfo.DB_CONFIG);
        Connection cnn = cm.borrow();
        String query = String.format(GET_LIST_QUERY, ConfigInfo.TABLE_USER, whereClause, EStatus.DELETE.getValue());
        try (PreparedStatement stmt = cnn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                TUser item = createFromReader(rs);
                if (item != null) {
                    lstValue.add(item);
                }
            }
            rs = stmt.executeQuery("SELECT FOUND_ROWS()");
            long totalRecord = 0;
            if (rs.next()) {
                totalRecord = rs.getLong(1);
            }
            //set data
            result.setStatus(totalRecord == 0? EStatusResult.FAIL : EStatusResult.OK);
            result.setListData(lstValue);
            result.setTotalRecord(totalRecord);
        } catch (SQLException ex) {
            logger.error(ex);
        } finally {
            cm.giveBack(cnn);
        }
        return result;
    }
    
}
