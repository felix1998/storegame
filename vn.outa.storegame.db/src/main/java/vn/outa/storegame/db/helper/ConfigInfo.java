package vn.outa.storegame.db.helper;

import vn.outa.framework.util.ConfigUtil;
import vn.outa.framework.util.ConvertUtil;

/**
 *
 * @author admin
 */
public class ConfigInfo {

    public static final String DB_CONFIG = "db_store_game";

    public static String TABLE_USER = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_user"), "tb_user");
    
    public static String TABLE_GAME = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_game"), "tb_game");
    
    public static String TABLE_GOLD_LOG = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_gold_log"), "tb_gold_log");
    
    public static String TABLE_GOLD_STATISTIC = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "tb_gold_statistic"), "tb_gold_statistic");
    
    public static String TABLE_CHEAT_TIENLEN_LOGS = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "cheat_tienlen_logs"), "cheat_tienlen_logs");
    
    public static String TABLE_BACCARAT_PAYOUT_CONTROL_LOGS = ConvertUtil.toString(ConfigUtil.getString(DB_CONFIG, "baccarat_payout_control_logs"), "baccarat_payout_control_logs");
}
