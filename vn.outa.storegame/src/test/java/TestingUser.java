
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thriftclient.UserClient;





/**
 *
 * @author admin
 */
public class TestingUser {
    

//    private static final UserClient clientUser = new UserClient(ConfigInfo.THRIFT_SERVICE);
//    
//    public static String thanhpho[] = {"ho chi minh", "ha noi", "da nang"};
//    public static String quan[] = {"quan 1", "quan 2", "quan 3", "quan 4", "quan 5", "quan 6", "quan 7", "quan 8", "quan 9"};
//    public static String phuong[] = {"phuong 1", "phuong 2", "phuong 3", "phuong 4", "phuong 5", "phuong 6", "phuong 7", "phuong 8", "phuong 9"};
//    public static String duong[] = {"duong so 1", "duong so 2", "duong so 3", "duong so 4", "duong so 5", "duong so 6", "duong so 7", "duong so 8", "duong so 9"};
//    public static Random ran = new Random();
   
    public static void main(String[] args) {
        

        /*       LOAD DU LIEU           */    
        
//        TUserListResult list = clientUser.getMulti(" and address like '%quan 7%tphcm%'", 0, 10);
//        for(TUser temp: list.listData){
//            System.out.println("user " + temp.getId() + ": " + temp.getUserName() + " - money: " + temp.getMoney());
//        }
//        
            
//        TUserListResult list;
//        System.out.println("-------------LOAD LAN 1------------------");
//        long startloadredis = System.currentTimeMillis();   
//        list = clientUser.getTopMoney(0, 10);
//        for(TUser temp: list.getListData()){
//            System.out.println("user " + temp.getId() + ": " + temp.getUserName() + " - money: " + temp.getMoney());
//        }
//        
//        long endloadredis = System.currentTimeMillis();
//        System.out.println("load redis top 10 in 1100000 row : time " + (endloadredis - startloadredis));

                /*       INSERT DU LIEU           */ 
//        long startout = System.currentTimeMillis();    
//        for(int i=1; i< 10; i++){
//            
//            TUser value = new TUser();
//            value.setUserName("nguoidung" + i);
//            value.setDisplayName("ten hien thi " + i);
//            value.setPassword("123");
//            value.setBirthDay(randomDOB());
//            value.setGender(ran.nextInt(2));
//            value.setRegisDate(String.valueOf(System.currentTimeMillis()));
//            value.setPhoneNumber(RandomStringUtils.randomNumeric(10));
//            value.setAddress(randomAddress());
//            value.setEmail("nguoidung" + i + "@gmail.com");
//            boolean rs = clientUser.create(value);
//            System.out.println("result: " + rs);
//        
//        }
//            int rs1 = clientUser.login("nguoidung1", "123");
//            System.out.println("login: " + rs1);
//            boolean rs1 = clientUser.logout("nguoidung1");
//            System.out.println("login: " + rs1);

//            TGame value = new TGame();
//            value.setGameName("maubinh");
//            value.setDisplayName("Mậu Binh Tính Chi");
//            value.setDecription("Bai Mậu Binh la mot game bai quoc te");
//            value.setStartDate("22/05/2022");
//            value.setStatus(1);
//            boolean rs = clientGame.create(value);
//            System.out.println("result: " + rs);
        
////        
//        long endout = System.currentTimeMillis();
//        System.out.println("insert 1000000 row : time " + (endout - startout));
        /*       UPDATE DU LIEU           */      
//        TUser valueUpdate = new TUser();
//        valueUpdate.setMoney(1000);
//        valueUpdate.setGold(2000);
//        valueUpdate.setId(12);
//        valueUpdate.setActive(1);
//        valueUpdate.setStatus(3);
////        boolean rsUpdate = clientUser.updateMoney(valueUpdate);
////        boolean rsUpdate = clientUser.updateActive(valueUpdate);
////        boolean rsUpdate = clientUser.updateGold(valueUpdate);
//        boolean rsUpdate = clientUser.updateStatus(valueUpdate);
//        System.out.println("result update: " + rsUpdate);
        
        /*       DELETE DU LIEU           */
//        boolean rsDelete = clientUser.remove(12);
//        System.out.println("delete 12: " + rsDelete);
        
//        System.out.println("-------------LOAD LAN 2------------------");
//        TUserListResult list = clientUser.getList(0, 5);
//        for(TUser temp: list.getListData()){
//            System.out.println("user " + temp.getId() + ": " + temp.getUserName() + " - gold: " + temp.getGold());
//        }

       
    }
    

//    public static String randomAddress(){
//        String result = ran.nextInt(1000) + ", " + duong[ran.nextInt(9)]+ ", " 
//                + phuong[ran.nextInt(9)] + ", " + quan[ran.nextInt(9)] + ", " + thanhpho[ran.nextInt(3)];
//        
//        return result;
//    }
//    
//    
//    public static String randomDOB() {
//
//        int yyyy = random(1900, 2013);
//        int mm = random(1, 12);
//        int dd = 0; // will set it later depending on year and month
//
//        switch(mm) {
//          case 2:
//            if (isLeapYear(yyyy)) {
//              dd = random(1, 29);
//            } else {
//              dd = random(1, 28);
//            }
//            break;
//
//          case 1:
//          case 3:
//          case 5:
//          case 7:
//          case 8:
//          case 10:
//          case 12:
//            dd = random(1, 31);
//            break;
//
//          default:
//            dd = random(1, 30);
//          break;
//        }
//
//        String year = Integer.toString(yyyy);
//        String month = Integer.toString(mm);
//        String day = Integer.toString(dd);
//
//        if (mm < 10) {
//            month = "0" + mm;
//        }
//
//        if (dd < 10) {
//            day = "0" + dd;
//        }
//
//        return day + '/' + month + '/' + year;
//    }
//
//    public static int random(int lowerBound, int upperBound) {
//        return (lowerBound + (int) Math.round(Math.random() * (upperBound - lowerBound)));
//    }
//
//    public static boolean isLeapYear(int year) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.YEAR, year);
//        int noOfDays = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
//
//        if (noOfDays > 365) {
//            return true;
//        }
//
//        return false;
//    }


}
