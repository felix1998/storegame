package vn.outa.storegame.business;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.GameCA;
import vn.outa.storegame.db.model.GameDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.game.EJobType;
import vn.outa.storegame.thrift.thrift.game.EStatus;
import vn.outa.storegame.thrift.thrift.game.EStatusResult;
import vn.outa.storegame.thrift.thrift.game.TGame;
import vn.outa.storegame.thrift.thrift.game.TGameListResult;
import vn.outa.storegame.thrift.thrift.game.TGameResult;

/**
 *
 * @author felix
 */
public class GameBC {
    
    private static final Logger logger = Logger.getLogger(GameBC.class);
    
    /**
     *  create data game
     *  Set default EStatus = ENABLE  
     *  @param value -TGame
     *  @author felix
     */
    public static boolean create(TGame value) {
        try {
            if (StringUtil.isNullOrEmpty(value.gameName) || StringUtil.isNullOrEmpty(value.displayName)) {
                return false;
            }
            if(checkExists(value.getGameName())){
                return false;
            }
            value.setId(getLastID() + 1);
            value.setStatus(EStatus.ENABLE.getValue());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GameCA.setLastID(value.getId());
            GameCA.setGame(value, true);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    /**
     *  update data Information data game
     *  @param value - TGame 
     *  @author felix
     */
    public static boolean updateInformation(TGame value) {
        try {
            if (StringUtil.isNullOrEmpty(value.displayName) || StringUtil.isNullOrEmpty(value.decription)
                    || StringUtil.isNullOrEmpty(value.startDate) ) {
                return false;
            }
            TGameResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setDisplayName(value.getDisplayName());
            valueUpdate.value.setDecription(value.getDecription());
            valueUpdate.value.setStartDate(value.getStartDate());
            valueUpdate.value.setEndDate(value.getEndDate());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_INFOR;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            GameCA.setGame(valueUpdate.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
     /**
     *  update EStatus game
     *  @param value - TGame
     *  @author felix
     */
    public static boolean updateStatus(TGame value) {
        try {
            TGameResult valueUpdate = getByIdNon(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setStatus(value.getStatus());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_STATUS;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            if(valueUpdate.value.getStatus() == EStatus.ENABLE.getValue()){
                GameCA.setGame(valueUpdate.value, true);
            } else {
                GameCA.setGame(valueUpdate.value, false);
                GameCA.removeItemInList(valueUpdate.value.id);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  remove data with id game
     *  remove true with EStatus = DISABLE and false with EStatus = ENABLE;
     *  @param id - long
     *  @author felix
     */
    public static boolean remove(long id) {
        try {
            TGameResult valueDelete = getByIdNon(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            //chi xoa cac data co status = delete
            if(valueDelete.value.getStatus() != EStatus.DELETE.getValue()){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;

            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GAME, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GameCA.removeGame(id);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     * check gameName in table game have exists
     * @param gameName - string
     * @return true - have data, false - NO data
     */
    public static boolean checkExists(String gameName) {
        return GameDA.getInstance().checkExists(gameName);
    }
    
    /**
     * get data game by id
     * EStatus = ENABLE
     * have redis
     * @param id - long
     * @return TGameResult
     */
    public static TGameResult getById(long id) {
        TGameResult result = null;
        try{
            TGame data = GameCA.getGame(id);
            if(data == null){
                result = GameDA.getInstance().getById(id);
                if(result.getStatus() == EStatusResult.OK){
                    GameCA.setGame(result.getValue(), true);
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TGameResult(EStatusResult.FAIL, null);
                return result;
            }
            result = new TGameResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data game by id
     * EStatus = ALL
     * have redis
     * @param id - long
     * @return TGameResult
     */
    public static TGameResult getByIdNon(long id) {
        TGameResult result = null;
        try{
            TGame data = GameCA.getGame(id);
            if(data == null){
                result = GameDA.getInstance().getByIdNon(id);
                if(result.getStatus() == EStatusResult.OK){
                    GameCA.setGame(result.getValue(), true);
                }
                return result;
            }
            result = new TGameResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data game by gameName
     * @param gameName - string in table game
     * @return TGameResult
     */
    public static TGameResult getByName(String gameName) {
        TGameResult result = GameDA.getInstance().getByName(gameName);
        return result;
    }
    
    /**
     * get all list data
     * have offset, count and redis
     * status = ENABLE  
     * @param offset - int 
     * @param count - int
     * @return TGameListResult
     */
    public static TGameListResult getList(int offset, int count) {
        TGameListResult result = new TGameListResult();
        
        try{
            List<Long> listId = GameCA.getList(offset, count);
            if(listId.isEmpty()){
                TGameListResult listData = GameDA.getInstance().getList("");
                for(TGame temp : listData.getListData()){
                    GameCA.addList(temp.getId(), temp.getId());
                }
                listId = GameCA.getList(offset, count);
            }
            
            List<TGame> list = new ArrayList<>();
            TGameResult data;
            for(Long id : listId){
                data = getById(id);
                if(data.getStatus() == EStatusResult.OK){
                    list.add(data.getValue());
                }
            }

            result.setListData(list);
            result.setTotalRecord(list.size());
            result.setStatus(result.getTotalRecord() == 0 ? EStatusResult.FAIL : EStatusResult.OK);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    
    /**
     * get all list data with whereClause
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGameListResult
     */
    public static TGameListResult getMulti(String whereClause, int offset, int count) {
        TGameListResult result = GameDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get list data with whereClause
     * status = DISABLE  
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGameListResult
     */
    public static TGameListResult getListDisable(String whereClause, int offset, int count) {
        TGameListResult result = GameDA.getInstance().getListDisable(whereClause);
        return result;
    }
    
    /**
     * get the last id in table game
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = GameCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = GameDA.getInstance().getLastID();
            GameCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
}
