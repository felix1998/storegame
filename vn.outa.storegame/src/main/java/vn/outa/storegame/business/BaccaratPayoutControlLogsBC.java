package vn.outa.storegame.business;

import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.BaccaratPayoutControlLogsCA;
import vn.outa.storegame.db.model.BaccaratPayoutControlLogsDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.EJobType;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogs;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsListResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsResult;

/**
 *
 * @author felix
 */
public class BaccaratPayoutControlLogsBC {
    
    private static final Logger logger = Logger.getLogger(BaccaratPayoutControlLogsBC.class);
    
    public static boolean create(TBaccaratPayoutControlLogs value) {
        try {
            value.setId(getLastID() + 1);
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_BACCARAT_PAYOUT_CONTROL_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            BaccaratPayoutControlLogsCA.setLastID(value.getId());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TBaccaratPayoutControlLogs value) {
        try {
            TBaccaratPayoutControlLogsResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.UPDATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_BACCARAT_PAYOUT_CONTROL_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    public static boolean remove(long id) {
        try {
            TBaccaratPayoutControlLogsResult valueDelete = getById(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_BACCARAT_PAYOUT_CONTROL_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    public static TBaccaratPayoutControlLogsResult getById(long id) {
        TBaccaratPayoutControlLogsResult result = BaccaratPayoutControlLogsDA.getInstance().getById(id);
        return result;
    }
    
    public static TBaccaratPayoutControlLogsListResult getMulti(String whereClause, int offset, int count) {
        TBaccaratPayoutControlLogsListResult result = BaccaratPayoutControlLogsDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get the last id in table BaccaratPayoutControlLogs
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = BaccaratPayoutControlLogsCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = BaccaratPayoutControlLogsDA.getInstance().getLastID();
            BaccaratPayoutControlLogsCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
}
