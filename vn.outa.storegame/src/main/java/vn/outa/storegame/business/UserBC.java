package vn.outa.storegame.business;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.UserCA;
import vn.outa.storegame.db.model.UserDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.user.EActive;
import vn.outa.storegame.thrift.thrift.user.EJobType;
import vn.outa.storegame.thrift.thrift.user.EStatus;
import vn.outa.storegame.thrift.thrift.user.EStatusResult;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thrift.user.TUserListResult;
import vn.outa.storegame.thrift.thrift.user.TUserResult;

/**
 *
 * @author felix
 */
public class UserBC {
    
    private static final Logger logger = Logger.getLogger(UserBC.class);
    
    /**
     *  create data user
     *  Set default EStatus = ENABLE  
     *  @param value -TUser
     *  @author felix
     */
    public static boolean create(TUser value) {
        try {
            if (StringUtil.isNullOrEmpty(value.userName) || StringUtil.isNullOrEmpty(value.password) 
                    || StringUtil.isNullOrEmpty(value.phoneNumber) || StringUtil.isNullOrEmpty(value.displayName)) {
                return false;
            }
            if(checkExists(value.userName, value.phoneNumber)){
                return false;
            }
            value.setId(getLastID() + 1);
            value.setLevel(0);
            value.setExperence(0);
            value.setWin(0);
            value.setLose(0);
            value.setQuit(0);
            value.setMoney(0);
            value.setGold(0);
            value.setActive(EActive.OFFLINE.getValue());
            value.setStatus(EStatus.ENABLE.getValue());
            value.setPassword(encodePassword(value.getPassword()));
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            UserCA.setLastID(value.getId());
            UserCA.setUser(value, true);
            UserCA.addUser(value.getUserName(), value.getId());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    /**
     *  update data Information in user
     *  @param value - TUser
     *  @author felix
     */
    public static boolean update(TUser value) {
        try {
            if (StringUtil.isNullOrEmpty(value.birthDay) || StringUtil.isNullOrEmpty(value.email)
                    || StringUtil.isNullOrEmpty(value.address) || value.gender < 0 ) {
                return false;
            }
            TUserResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            if(valueUpdate.value.getActive() == EActive.OFFLINE.getValue()){
                return false;
            }
            valueUpdate.value.setBirthDay(value.getBirthDay());
            valueUpdate.value.setGender(value.getGender());
            valueUpdate.value.setAddress(value.getAddress());
            valueUpdate.value.setEmail(value.getEmail());

            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueUpdate.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update money in data user 
     *  @param value - TUser
     *  @author felix
     */
    public static boolean updateMoney(TUser value) {
        try {
            TUserResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            if(valueUpdate.value.getActive() == EActive.OFFLINE.getValue()){
                return false;
            }
            valueUpdate.value.setMoney(value.getMoney());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_MONEY;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueUpdate.value, false);
            if(UserCA.isListMoney()){
                UserCA.addListMoney(valueUpdate.value.getMoney(), valueUpdate.value.getId());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update gold in data user 
     *  @param value - TUser
     *  @author felix
     */
    public static boolean updateGold(TUser value) {
        try {
            TUserResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            if(valueUpdate.value.getActive() == EActive.OFFLINE.getValue()){
                return false;
            }
            valueUpdate.value.setGold(value.getGold());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_GOLD;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueUpdate.value, false);
            if(UserCA.isListGold()){
                UserCA.addListGold(valueUpdate.value.getGold(), valueUpdate.value.getId());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
     /**
     *  update EStatus user
     *  @param value - TUser
     *  @author felix
     */
    public static boolean updateStatus(TUser value) {
        try {
            TUserResult valueUpdate = getByIdNon(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setStatus(value.getStatus());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_STATUS;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            if(valueUpdate.value.getStatus() == EStatus.ENABLE.getValue()){
                UserCA.setUser(valueUpdate.value, true);
            } else {
                UserCA.setUser(valueUpdate.value, false);
                UserCA.removeItemInList(valueUpdate.value);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update Active user
     *  @param value - TUser
     *  @author felix
     */
    public static boolean updateActive(TUser value) {
        try {
            TUserResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setActive(value.getActive());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_ACTIVE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueUpdate.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update password user
     *  update if userName true, oldPassword true and active == ONLINE
     *  @param userName - String
     *  @param oldPassword - String
     *  @param newPassword - String
     *  @author felix
     */
    public static boolean updatePassword(String userName, String oldPassword, String newPassword) {
        try {
            TUserResult valueUpdate = getByUserName(userName);
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            if(!valueUpdate.value.getPassword().equals(encodePassword(oldPassword)) 
                    || valueUpdate.value.getActive()== EActive.OFFLINE.getValue()){
                return false;
            }
            valueUpdate.value.setPassword(encodePassword(newPassword));
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_PASSWORD;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueUpdate.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update Synchronic gold in user from GoldLog
     *  @param userName - string in user
     *  @param goldChange - double gold change
     *  @author felix
     */
    public static boolean updateGoldSyn(String userName, double goldChange) {
        try {
            TUserResult valueSyn = getByUserName(userName);
            if(valueSyn.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueSyn.value.setGold(valueSyn.value.getGold() + goldChange);
                
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.type = EJobType.UPDATE_GOLD;
            job.data = JsonUtil.serialize(valueSyn.value);
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueSyn.value, false);
            if(UserCA.isListGold()){
                UserCA.addListGold(valueSyn.value.getGold(), valueSyn.value.getId());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    /**
     *  remove data with id user
     *  ONLY remove with EStatus = DELETE (TRUE)
     *  false if EStatus = DISABLE and EStatus = ENABLE;
     *  @param id - long
     *  @author felix
     */
    public static boolean remove(long id) {
        try {
            TUserResult valueDelete = getByIdNon(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            //chi xoa cac user co status = delete
            if(valueDelete.value.getStatus() != EStatus.DELETE.getValue()){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            UserCA.removeUser(valueDelete.value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    
    /**
     * login user with userName and Password
     * have redis
     * update active ONLINE if login thanh cong
     * @param userName - string
     * @param password - string
     * @return int  0 - (login thanh cong)
     *              1 - (khong ton tai tai khoan userName error)
     *              2 - (co tai khoan nhung password sai error)
     */
    public static int login(String userName, String password) {
        try {
            TUserResult valueLogin = getByUserName(userName);
            if(valueLogin.getStatus() == EStatusResult.FAIL){
                return 1;
            }
            if(!valueLogin.value.getPassword().equals(encodePassword(password))){
                return 2;
            }
            
            valueLogin.value.setActive(EActive.ONLINE.getValue());
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueLogin.value);
            job.type = EJobType.UPDATE_ACTIVE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return 3;
            }

            UserCA.setUser(valueLogin.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return 0;
    }
    
    /**
     * logout user with userName
     * have redis
     * update active OFFLINE
     * update lastLogin
     * @param userName - string
     */
    public static boolean logout(String userName) {
        try {
            TUserResult valueLogout = getByUserName(userName);
            if(valueLogout.getStatus() == EStatusResult.FAIL){
                return false;
            }
            if(valueLogout.value.getActive() != EActive.ONLINE.getValue()){
                return false;
            }
            valueLogout.value.setActive(EActive.OFFLINE.getValue());
            valueLogout.value.setLastLogin(String.valueOf(System.currentTimeMillis()));
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueLogout.value);
            job.type = EJobType.LOGOUT;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_USER, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            UserCA.setUser(valueLogout.value, false);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    public static boolean checkExists(String userName, String phoneNumber) {
        return UserDA.getInstance().checkExists(userName, phoneNumber);
    }
    
    /**
     * get data user with id
     * have redis
     * @param id - long
     * @return TUserResult
     */
    public static TUserResult getById(long id) {
        TUserResult result = null;
        try{
            TUser data = UserCA.getUser(id);
            if(data == null){
                result = UserDA.getInstance().getById(id);
                if(result.getStatus() == EStatusResult.OK){
                    UserCA.setUser(result.getValue(), false);
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TUserResult(EStatusResult.FAIL, null);
                return result;
            }
            result = new TUserResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    /**
     * get data user with id
     * have redis
     * @param id - long
     * @return TUserResult
     */
    public static TUserResult getByIdNon(long id) {
        TUserResult result = null;
        try{
            TUser data = UserCA.getUser(id);
            if(data == null){
                result = UserDA.getInstance().getByIdNon(id);
                if(result.getStatus() == EStatusResult.OK){
                    UserCA.setUser(result.getValue(), false);
                }
                return result;
            }
            result = new TUserResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data with userName in user
     * status = ENABLE
     * have redis
     * @param userName - string data in user
     * @return TUserResult
     */
    public static TUserResult getByUserName(String userName) {
        TUserResult result = null;
        try{
            TUser data = UserCA.getUser(userName);
            if(data == null){
                result = UserDA.getInstance().getByUserName(userName);
                if(result.getStatus() == EStatusResult.OK){
                    UserCA.setUser(result.value, false);
                    UserCA.addUser(userName, result.value.getId());
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TUserResult(EStatusResult.FAIL, null);
                return result;
            }
            result = new TUserResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data user by userName or phoneNumber
     * @param keyword - string search in table user
     * @return TUserResult
     */
    public static TUserResult getByUserOrPhone(String keyword) {
        TUserResult result = UserDA.getInstance().getByUserOrPhone(keyword);
        return result;
    }
    
    /**
     * get all list data
     * have offset, count and redis
     * status = ENABLE  
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getList(int offset, int count) {
        TUserListResult result = new TUserListResult();
        
        try{
            List<Long> listId = UserCA.getList(offset, count);
            if(listId.isEmpty()){
                result = UserDA.getInstance().getList("");
                if(result.getStatus() == EStatusResult.FAIL){
                    return result;
                }
                for(TUser temp : result.listData){
                    UserCA.addList(temp.getId(), temp.getId());
                }
                listId = UserCA.getList(offset, count);
                if(listId.isEmpty()){
                    result = new TUserListResult(EStatusResult.FAIL, null, 0);
                    return result;
                }
            }
            
            List<TUser> list = new ArrayList<>();
            TUserResult data;
            for(Long id : listId){
                data = getById(id);
                if(data.getStatus() == EStatusResult.OK){
                    list.add(data.value);
                }
            }

            result.setListData(list);
            result.setTotalRecord(list.size());
            result.setStatus(EStatusResult.OK);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get list top gold
     * have offset, count and redis
     * status = ENABLE  
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getTopGold(int offset, int count) {
        TUserListResult result = new TUserListResult();
        
        try{
            List<Long> listId = UserCA.getListGold(offset, count);
            if(listId.isEmpty()){
                result = UserDA.getInstance().getList("");
                if(result.getStatus() == EStatusResult.FAIL){
                    return result;
                }
                for(TUser temp : result.listData){
                    UserCA.addListGold(temp.getGold(), temp.getId());
                }
                listId = UserCA.getListGold(offset, count);
                if(listId.isEmpty()){
                    result = new TUserListResult(EStatusResult.FAIL, null, 0);
                    return result;
                }
            }
            
            List<TUser> list = new ArrayList<>();
            TUserResult data;
            for(Long id : listId){
                data = getById(id);
                if(data.getStatus() == EStatusResult.OK){
                    list.add(data.value);
                }
            }

            result.setListData(list);
            result.setTotalRecord(list.size());
            result.setStatus(EStatusResult.OK);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get list top money
     * have offset, count and redis
     * status = ENABLE  
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getTopMoney(int offset, int count) {
        TUserListResult result = new TUserListResult();
        
        try{
            List<Long> listId = UserCA.getListMoney(offset, count);
            if(listId.isEmpty()){
                result = UserDA.getInstance().getList("");
                if(result.getStatus() == EStatusResult.FAIL){
                    return result;
                }
                for(TUser temp : result.listData){
                    UserCA.addListMoney(temp.getMoney(), temp.getId());
                }
                listId = UserCA.getListMoney(offset, count);
                if(listId.isEmpty()){
                    result = new TUserListResult(EStatusResult.FAIL, null, 0);
                    return result;
                }
            }
            
            List<TUser> list = new ArrayList<>();
            TUserResult data;
            for(Long id : listId){
                data = getById(id);
                if(data.getStatus() == EStatusResult.OK){
                    list.add(data.value);
                }
            }

            result.setListData(list);
            result.setTotalRecord(list.size());
            result.setStatus(EStatusResult.OK);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }

    /**
     * get all list data with whereClause
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getMulti(String whereClause, int offset, int count) {
        TUserListResult result = UserDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get list data with whereClause
     * status = DISABLE  
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getListDisable(String whereClause, int offset, int count) {
        TUserListResult result = UserDA.getInstance().getListDisable(whereClause);
        return result;
    }
     
    /**
     * get list data with whereClause
     * status = DELETE  
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TUserListResult
     */
    public static TUserListResult getListDelete(String whereClause, int offset, int count) {
        TUserListResult result = UserDA.getInstance().getListDelete(whereClause);
        return result;
    }
    
    
    /**
     * get the last id in table user
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = UserCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = UserDA.getInstance().getLastID();
            UserCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    public static String encodePassword (String password) {
        String result = "";
        try{
            password = password + "@felix";
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] digest = md.digest();
            result = DatatypeConverter.printHexBinary(digest).toUpperCase();
            //encode lan 2
            result = result + "@outa";
            md.update(result.getBytes());
            digest = md.digest();
            result = DatatypeConverter.printHexBinary(digest).toUpperCase();
        }catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
}
