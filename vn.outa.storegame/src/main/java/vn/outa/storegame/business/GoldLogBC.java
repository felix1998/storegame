package vn.outa.storegame.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.GoldLogCA;
import vn.outa.storegame.db.model.GoldLogDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.goldLog.EJobType;
import vn.outa.storegame.thrift.thrift.goldLog.EStatus;
import vn.outa.storegame.thrift.thrift.goldLog.EStatusResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLog;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogListResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogResult;

/**
 *
 * @author felix
 */
public class GoldLogBC {
    
    private static final Logger logger = Logger.getLogger(GoldLogBC.class);
    
    /**
     *  create data GoldLog
     *  Set default EStatus = ENABLE  
     *  update Syn with GoldStatistic
     *  @param value -TGoldLog
     *  @author felix
     */
    public static boolean create(TGoldLog value) {
        try {
            if (StringUtil.isNullOrEmpty(value.userName) || StringUtil.isNullOrEmpty(value.gameName) || value.date < 0 ) {
                return false;
            }
            value.setId(getLastID() + 1);
            value.setStatus(EStatus.ENABLE.getValue());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_LOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldLogCA.setLastID(value.getId());
            GoldLogCA.setGoldLog(value);
            //syn with goldStatistic and user
            GoldStatisticBC.updateSynchronic(value.getUserName(), value.getGameName(), value.getDate(), value.getGoldChange());
            UserBC.updateGoldSyn(value.getUserName(), value.getGoldChange());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    /**
     *  update goldChange and update Syn with GoldStatistic 
     *  @param value - TGlodLog 
     *  @author felix
     */
    public static boolean updateGold(TGoldLog value) {
        try {
            TGoldLogResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            double goldUpdate = -valueUpdate.value.getGoldChange() + value.getGoldChange();
            valueUpdate.value.setGoldChange(value.getGoldChange());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_GOLD;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_LOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            GoldLogCA.setGoldLog(valueUpdate.value);
            //syn with goldStatistic and user
            GoldStatisticBC.updateSynchronic(valueUpdate.value.getUserName(), valueUpdate.value.getGameName(), valueUpdate.value.getDate(), goldUpdate);
            UserBC.updateGoldSyn(value.getUserName(), goldUpdate);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update EStatus and Syn with GoldStatistic
     *  update totalGold in GoldStatistic
     *  @param value - TGlodLog
     *  @author felix
     */
    public static boolean updateStatus(TGoldLog value) {
        try {
            TGoldLogResult valueUpdate = getByIdNon(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setStatus(value.getStatus());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_STATUS;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_LOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldLogCA.setGoldLog(valueUpdate.value);
            //syn with goldStatistic and user
            valueUpdate.value.setGoldChange(-valueUpdate.value.getGoldChange());
            GoldStatisticBC.updateSynchronic(valueUpdate.value.getUserName(), valueUpdate.value.getGameName(), valueUpdate.value.getDate(), valueUpdate.value.getGoldChange());
            UserBC.updateGoldSyn(value.getUserName(), valueUpdate.value.getGoldChange());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  remove goldLog with id GoldLog
     *  remove true with EStatus = DISABLE and false with EStatus = ENABLE;
     *  @param id - long
     *  @author felix
     */
    public static boolean remove(long id) {
        try {
            TGoldLogResult valueDelete = getByIdNon(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            //chi xoa cac data da disable
            if(valueDelete.value.getStatus() != EStatus.DISABLE.getValue()){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_LOG, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldLogCA.removeGoldLog(valueDelete.value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     * get data goldLog with id
     * status = ENBALE
     * have redis
     * @param id - long
     * @return TGoldLogResult
     */
    public static TGoldLogResult getById(long id) {
        TGoldLogResult result = null;
        try{
            TGoldLog data = GoldLogCA.getGoldLog(id);
            if(data == null){
                result = GoldLogDA.getInstance().getById(id);
                if(result.getStatus() == EStatusResult.OK){
                    GoldLogCA.setGoldLog(result.getValue());
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TGoldLogResult(EStatusResult.FAIL, null);
                return result;
            }
            result = new TGoldLogResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data goldLog with id
     * status = ALL
     * have redis
     * @param id - long
     * @return TGoldLogResult
     */
    public static TGoldLogResult getByIdNon(long id) {
        TGoldLogResult result = null;
        try{
            TGoldLog data = GoldLogCA.getGoldLog(id);
            if(data == null){
                result = GoldLogDA.getInstance().getByIdNon(id);
                if(result.getStatus() == EStatusResult.OK){
                    GoldLogCA.setGoldLog(result.getValue());
                }
                return result;
            }
            result = new TGoldLogResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
   
    /**
     * get list data with userName
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getListByUser(String userName, int offset, int count) {
        TGoldLogListResult result = GoldLogDA.getInstance().getListByUser(userName);
        return result;
    }
    
    /**
     * get list data with userName and gameName
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param gameName - string data in game
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getListByUserGame(String userName, String gameName, int offset, int count) {
        TGoldLogListResult result = GoldLogDA.getInstance().getListByUserGame(userName, gameName);
        return result;
    }
    
    /**
     * get list data with userName and day
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param day - long
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getListByUserDay(String userName, long day, int offset, int count) {
        TGoldLogListResult result = GoldLogDA.getInstance().getListByUser(userName);
        return result;
    }
    
    /**
     * get list data with userName, gameName and day
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param gameName - string data in game
     * @param day - long
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getListByUserGameDay(String userName, String gameName, long day, int offset, int count) {
        List<Long> listDay = getBeginEndDay(day);
        long beginDay = listDay.get(0);
        long endDay = listDay.get(1);
        TGoldLogListResult result = GoldLogDA.getInstance().getListByUserGameDay(userName, gameName, beginDay, endDay);
        return result;
    }
    
    /**
     * get all list data with whereClause
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getMulti(String whereClause, int offset, int count) {
        TGoldLogListResult result = GoldLogDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get list data with whereClause
     * status = DISABLE  
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGoldLogListResult
     */
    public static TGoldLogListResult getListDisable(String whereClause, int offset, int count) {
        TGoldLogListResult result = GoldLogDA.getInstance().getListDisable(whereClause);
        return result;
    }
    
     /**
     * get the last id in table goldLog
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = GoldLogCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = GoldLogDA.getInstance().getLastID();
            GoldLogCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * convert day begin and end with day input
     * @param day - long
     * @return list<Long> (time dayBegin and time dayEnd) 
     */
    public static List<Long> getBeginEndDay (long day){
        List<Long> result =  new ArrayList<>();
        Date todayBegin = new Date(day - day % (24 * 60 * 60 * 1000));               /* set day ve time bat dau ngay hom nay 07:00:00 theo local vn +7*/
        Date todayEnd = new Date((todayBegin.getTime() + 24 * 60 * 60 * 1000)-1);    /* set day ve time ket thuc ngay hom nay 06:59:59 theo local vn +7*/
        
        result.add(todayBegin.getTime());
        result.add(todayEnd.getTime());
        return result;
    }
    
}
