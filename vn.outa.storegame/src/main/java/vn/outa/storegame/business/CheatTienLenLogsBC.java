package vn.outa.storegame.business;

import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.CheatTienLenLogsCA;
import vn.outa.storegame.db.model.CheatTienLenLogsDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.EJobType;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogs;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsListResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsResult;

/**
 *
 * @author felix
 */
public class CheatTienLenLogsBC {
    
    private static final Logger logger = Logger.getLogger(CheatTienLenLogsBC.class);
    
    public static boolean create(TCheatTienLenLogs value) {
        try {
            
            value.setId(getLastID() + 1);
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_CHEAT_TIENLEN_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            CheatTienLenLogsCA.setLastID(value.getId());
            CheatTienLenLogsCA.setCheatTienLenLogs(value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    public static boolean update(TCheatTienLenLogs value) {
        try {
            TCheatTienLenLogsResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }

            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.UPDATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_CHEAT_TIENLEN_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            CheatTienLenLogsCA.setCheatTienLenLogs(value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    public static boolean remove(long id) {
        try {
            TCheatTienLenLogsResult valueDelete = getById(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_CHEAT_TIENLEN_LOGS, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            CheatTienLenLogsCA.removeCheatTienLenLogs(valueDelete.value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    public static TCheatTienLenLogsResult getById(long id) {
        TCheatTienLenLogsResult result = null;
        try{
            TCheatTienLenLogs data = CheatTienLenLogsCA.getCheatTienLenLogs(id);
            if(data == null){
                result = CheatTienLenLogsDA.getInstance().getById(id);
                if(result.getStatus() == EStatusResult.OK){
                    CheatTienLenLogsCA.setCheatTienLenLogs(result.getValue());
                }
                return result;
            }
            result = new TCheatTienLenLogsResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    public static TCheatTienLenLogsListResult getMulti(String whereClause, int offset, int count) {
        TCheatTienLenLogsListResult result = CheatTienLenLogsDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get the last id in table CheatTienLenLogs
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = CheatTienLenLogsCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = CheatTienLenLogsDA.getInstance().getLastID();
            CheatTienLenLogsCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
}
