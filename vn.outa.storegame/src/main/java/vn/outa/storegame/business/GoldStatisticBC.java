package vn.outa.storegame.business;

import java.util.Date;
import org.apache.log4j.Logger;
import org.gearman.client.GearmanJob;
import vn.outa.framework.gearman.GClientManager;
import vn.outa.framework.gearman.JobEnt;
import vn.outa.framework.util.JsonUtil;
import vn.outa.framework.util.StringUtil;
import vn.outa.framework.util.UuidUtil;
import vn.outa.storegame.cache.GoldStatisticCA;
import vn.outa.storegame.db.model.GoldStatisticDA;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.thrift.goldstatistic.EJobType;
import vn.outa.storegame.thrift.thrift.goldstatistic.EStatus;
import vn.outa.storegame.thrift.thrift.goldstatistic.EStatusResult;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticListResult;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticResult;

/**
 *
 * @author felix
 */
public class GoldStatisticBC {
    
    private static final Logger logger = Logger.getLogger(GoldStatisticBC.class);
    
    /**
     *  create data GoldStatistic
     *  Set default EStatus = ENABLE  
     *  @param value - TGoldStatistic
     *  @author felix
     */
    public static boolean create(TGoldStatistic value) {
        try {
            if (StringUtil.isNullOrEmpty(value.userName) || StringUtil.isNullOrEmpty(value.gameName)
                    || StringUtil.isNullOrEmpty(value.day) ) {
                return false;
            }
            value.setId(getLastID() + 1);
            value.setStatus(EStatus.ENABLE.getValue());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.CREATE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldStatisticCA.setLastID(value.getId());
            GoldStatisticCA.setGoldStatistic(value);
            GoldStatisticCA.addGoldStatistic(value.getUserName(), value.getGameName(), value.getDay(), value.getId());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }

    /**
     *  update totalGold
     *  @param value - TGoldStatistic 
     *  @author felix
     */
    public static boolean updateGold(TGoldStatistic value) {
        try {
            TGoldStatisticResult valueUpdate = getById(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setTotalGold(value.getTotalGold());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(valueUpdate.value);
            job.type = EJobType.UPDATE_GOLD;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }

            GoldStatisticCA.setGoldStatistic(valueUpdate.value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update EStatus
     *  @param value TGoldStatistic
     *  @author felix
     */
    public static boolean updateStatus(TGoldStatistic value) {
        try {
            TGoldStatisticResult valueUpdate = getByIdNon(value.getId());
            if(valueUpdate.getStatus() == EStatusResult.FAIL){
                return false;
            }
            valueUpdate.value.setStatus(value.getStatus());
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(value);
            job.type = EJobType.UPDATE_STATUS;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldStatisticCA.setGoldStatistic(value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  update Synchronic total_gold in goldStatistic from GoldLog
     *  @param userName - string in user
     *  @param gameName - string in game
     *  @param day - string day format dd/mm/yyyy
     *  @param goldChange - double gold change
     *  @author felix
     */
    public static boolean updateSynchronic(String userName, String gameName, long day, double goldChange) {
        try {
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            
            TGoldStatisticResult valueSyn = getByUserGameDay(userName, gameName, day);
            if(valueSyn.getStatus() == EStatusResult.FAIL){
                String beginDay = String.valueOf(getBeginDay(day));
                valueSyn.value = new TGoldStatistic();
                valueSyn.value.setUserName(userName);
                valueSyn.value.setGameName(gameName);
                valueSyn.value.setDay(beginDay);
                valueSyn.value.setTotalGold(goldChange);
                valueSyn.value.setStatus(EStatus.ENABLE.getValue());
                valueSyn.value.setId(getLastID() + 1);
                
                job.type = EJobType.CREATE;
                job.data = JsonUtil.serialize(valueSyn.value);
                if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                    return false;
                }
                
                GoldStatisticCA.setLastID(valueSyn.value.getId());
                GoldStatisticCA.addGoldStatistic(valueSyn.value.getUserName(), valueSyn.value.getGameName(), valueSyn.value.getDay(), valueSyn.value.getId());
            }else {
                valueSyn.value.setTotalGold(valueSyn.value.totalGold + goldChange);
                
                job.type = EJobType.UPDATE_GOLD;
                job.data = JsonUtil.serialize(valueSyn.value);
                if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                    return false;
                }
            }
            
            GoldStatisticCA.setGoldStatistic(valueSyn.value);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
    /**
     *  remove goldStatistic with id GoldStatistic
     *  remove true with EStatus = DISABLE and false with EStatus = ENABLE;
     *  @param id - long
     *  @author felix
     */
    public static boolean remove(long id) {
        try {
            TGoldStatisticResult valueDelete = getByIdNon(id);
            if(valueDelete.getStatus() == EStatusResult.FAIL){
                return false;
            }
            //chi xoa cac data da disable
            if(valueDelete.value.getStatus() != EStatus.DISABLE.getValue()){
                return false;
            }
            
            JobEnt job = new JobEnt();
            job.uuid = UuidUtil.makeString();
            job.data = JsonUtil.serialize(id);
            job.type = EJobType.DELETE;
            if (!GClientManager.getInstance(ConfigInfo.GEARMAN_SERVER).push(job, ConfigInfo.FUNCTION_GOLD_STATISTIC, GearmanJob.JobPriority.NORMAL)) {
                return false;
            }
            
            GoldStatisticCA.removeGoldStatistic(valueDelete.getValue());
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return true;
    }
    
     /**
     * get data goldLog with id
     * status = ENABLE
     * have redis
     * @param id - int
     * @return TGoldStatisticResult
     */
    public static TGoldStatisticResult getById(long id) {
        TGoldStatisticResult result = null;
        try{
            TGoldStatistic data = GoldStatisticCA.getGoldStatistic(id);
            if(data == null){
                result = GoldStatisticDA.getInstance().getById(id);
                if(result.getStatus() == EStatusResult.OK){
                    GoldStatisticCA.setGoldStatistic(result.getValue());
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TGoldStatisticResult(EStatusResult.FAIL, null);
                return result;
            }
            result = new TGoldStatisticResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
     
    /**
     * get data goldLog with id
     * status = ALL
     * have redis
     * @param id - int
     * @return TGoldStatisticResult
     */
    public static TGoldStatisticResult getByIdNon(long id) {
        TGoldStatisticResult result = null;
        try{
            TGoldStatistic data = GoldStatisticCA.getGoldStatistic(id);
            if(data == null){
                result = GoldStatisticDA.getInstance().getByIdNon(id);
                if(result.getStatus() == EStatusResult.OK){
                    GoldStatisticCA.setGoldStatistic(result.getValue());
                }
                return result;
            }
            
            result = new TGoldStatisticResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get data with userName and gameName and day
     * status = ENABLE  
     * have redis
     * @param userName - string data in user
     * @param gameName - string data in game
     * @param day - string day format dd/mm/yyyy
     * @return TGoldStatisticResult
     */
    public static TGoldStatisticResult getByUserGameDay(String userName, String gameName, long day) {
        TGoldStatisticResult result = null;
        try{
            String beginDay = String.valueOf(getBeginDay(day));
            TGoldStatistic data = GoldStatisticCA.getGoldStatistic(userName, gameName, beginDay);
            if(data == null){
                result = GoldStatisticDA.getInstance().getByUserGameDay(userName, gameName, beginDay);
                if(result.getStatus() == EStatusResult.OK){
                    GoldStatisticCA.setGoldStatistic(result.value);
                    GoldStatisticCA.addGoldStatistic(result.value.getUserName(), result.value.getGameName(), beginDay, result.value.getId());
                }
                return result;
            }
            if(data.getStatus() != EStatus.ENABLE.getValue()){
                result = new TGoldStatisticResult(EStatusResult.FAIL, null);
                return result;
            }
            
            result = new TGoldStatisticResult(EStatusResult.OK, data);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * get list data with userName
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param offset - int 
     * @param count - int
     * @return TGoldStatisticListResult
     */
    public static TGoldStatisticListResult getListByUser(String userName, int offset, int count) {
        TGoldStatisticListResult result = GoldStatisticDA.getInstance().getListByUser(userName);
        return result;
    }
    
    /**
     * get list data with userName and gameName
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param gameName - string data in game
     * @param offset - int 
     * @param count - int
     * @return TGoldStatisticListResult
     */
    public static TGoldStatisticListResult getListByUserGame(String userName, String gameName, int offset, int count) {
        TGoldStatisticListResult result = GoldStatisticDA.getInstance().getListByUserGame(userName, gameName);
        return result;
    }
    
    /**
     * get list data with userName and day
     * have offset, count
     * status = ENABLE  
     * @param userName - string data in user
     * @param day - string format dd/mm/yyyy
     * @param offset - int 
     * @param count - int
     * @return TGoldStatisticListResult
     */
    public static TGoldStatisticListResult getListByUserDay(String userName, long day, int offset, int count) {
        TGoldStatisticListResult result = GoldStatisticDA.getInstance().getListByUserDay(userName, String.valueOf(getBeginDay(day)));
        return result;
    }
    
    /**
     * get all list data with whereClause
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGoldStatisticListResult
     */
    public static TGoldStatisticListResult getMulti(String whereClause, int offset, int count) {
        TGoldStatisticListResult result = GoldStatisticDA.getInstance().getListMulti(whereClause);
        return result;
    }
    
     /**
     * get list data with whereClause
     * status = DISABLE  
     * @param whereClause - string
     * @param offset - int 
     * @param count - int
     * @return TGoldStatisticListResult
     */
    public static TGoldStatisticListResult getListDisable(String whereClause, int offset, int count) {
        TGoldStatisticListResult result = GoldStatisticDA.getInstance().getListDisable(whereClause);
        return result;
    }
    
    /**
     * get the last id in table goldStatistic
     * have redis
     * @return last_id - long
     */
    public static Long getLastID (){
        Long result = null;
        try{
            result = GoldStatisticCA.getLastID();
            if(result > 0){
                return result;
            }
            
            result = GoldStatisticDA.getInstance().getLastID();
            GoldStatisticCA.setLastID(result);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
    /**
     * convert day begin with day input
     * @param day - long
     * @return result - time dayBegin
     */
    public static Long getBeginDay (long day){
        /* set day ve time bat dau ngay hom do 07:00:00 theo local vn +7*/
        Long result = new Date(day - day % (24 * 60 * 60 * 1000)).getTime();
        return result;
    }
   
}
