package vn.outa.storegame.deamon;

import org.apache.log4j.Logger;
import vn.outa.framework.thrift.server.TServerFactory;
import vn.outa.framework.thrift.server.TServerManager;
import vn.outa.framework.thrift.server.TServerProcessor;
import vn.outa.framework.util.LogUtil;
import vn.outa.storegame.helper.ConfigInfo;
import vn.outa.storegame.thrift.BaccaratPayoutControlLogsProcessor;
import vn.outa.storegame.thrift.CheatTienLenLogsProcessor;
import vn.outa.storegame.thrift.GameProcessor;
import vn.outa.storegame.thrift.GoldLogProcessor;
import vn.outa.storegame.thrift.GoldStatisticProcessor;
import vn.outa.storegame.thrift.UserProcessor;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.BaccaratPayoutControlLogsService;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.CheatTienLenLogsService;
import vn.outa.storegame.thrift.thrift.game.GameService;
import vn.outa.storegame.thrift.thrift.goldLog.GoldLogService;
import vn.outa.storegame.thrift.thrift.goldstatistic.GoldStatisticService;
import vn.outa.storegame.thrift.thrift.user.UserService;

/**
 *
 * @author admin
 */
public class RunService {

    private static final Logger logger = LogUtil.getLogger(RunService.class);

    public static void main(String[] args) throws Exception {

        TServerFactory tsf = new TServerFactory(ConfigInfo.THRIFT_SERVICE);
        tsf.addProcessor(new TServerProcessor(UserService.class, UserProcessor.class));
        tsf.addProcessor(new TServerProcessor(GameService.class, GameProcessor.class));
        tsf.addProcessor(new TServerProcessor(GoldLogService.class, GoldLogProcessor.class));
        tsf.addProcessor(new TServerProcessor(GoldStatisticService.class, GoldStatisticProcessor.class));
        tsf.addProcessor(new TServerProcessor(CheatTienLenLogsService.class, CheatTienLenLogsProcessor.class));
        tsf.addProcessor(new TServerProcessor(BaccaratPayoutControlLogsService.class, BaccaratPayoutControlLogsProcessor.class));

        TServerManager server = new TServerManager();
        server.add(tsf);
        server.start();
        logger.info("Thrift server start!");
        
    }
}
