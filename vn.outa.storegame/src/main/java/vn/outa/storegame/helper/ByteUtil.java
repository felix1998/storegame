package vn.outa.storegame.helper;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
/**
 *
 * @author felix
 */
public class ByteUtil {

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE / Byte.SIZE);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE / Byte.SIZE);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getLong();
    }

    public static String bytesToString(byte[] bytes) {
        return new String(bytes, Charset.defaultCharset().forName("UTF-8"));
    }

    public static byte[] stringToBytes(String string) {
        return string.getBytes(Charset.forName("UTF-8"));
    }
}