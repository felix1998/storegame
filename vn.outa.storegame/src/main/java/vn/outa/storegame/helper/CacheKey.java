package vn.outa.storegame.helper;

/**
 *
 * @author felix
 */
public class CacheKey {
    
    /**
     * Database name key.
     */
    public static final String DB = "db_store_game:";
    
    /**
     * User.
     */
    public static final String USER_KEY = DB + "user:%s";
    
    public static final String USER_LASTID_KEY = DB + "user:lastID";
    
    public static final String USER_LIST_KEY = DB + "user_list";
    
    public static final String USER_LIST_GOLD_KEY = DB + "user_list_gold";
    
    public static final String USER_LIST_MONEY_KEY = DB + "user_list_money";
    
    public static final String USER_LIST_NAME_KEY = DB + "user_list_name";
    
    /**
     * Game.
     */
    public static final String GAME_KEY = DB + "game:%s";
    
    public static final String GAME_LASTID_KEY = DB + "game:lastID";
    
    public static final String GAME_LIST_KEY = DB + "game_list";
    
    /**
     * GoldLog.
     */
    public static final String GOLD_LOG_KEY = DB + "gold_log:%s";
    
    public static final String GOLD_LOG_LASTID_KEY = DB + "gold_log:lastID";
    

    /**
     * GoldStatistic.
     */
    public static final String GOLD_STATISTIC_KEY = DB + "gold_statistic:%s";
    
    public static final String GOLD_STATISTIC_LASTID_KEY = DB + "gold_statistic:lastID";
    
    public static final String GOLD_STATISTIC_LIST_SYN_KEY = DB + "gold_statistic_list_syn";
    
    /**
     * CheatTienlenLog.
     */
    public static final String CHEAT_TIENLEN_LOGS_KEY = DB + "cheat_tienlen_logs:%s";
    
    public static final String CHEAT_TIENLEN_LOGS_LASTID_KEY = DB + "cheat_tienlen_logs:lastID";
    
    /**
     * BaccaratPayoutControlLog.
     */
    public static final String BACCARAT_PAYOUT_CONTROL_LOGS_LASTID_KEY = DB + "baccarat_payout_control_logs:lastID";
    
}
