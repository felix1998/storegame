package vn.outa.storegame.helper;

import vn.outa.framework.util.ConvertUtil;
import vn.outa.storegame.utils.ConfigUtil;


/**
 *
 * @author admin
 */
public class ConfigInfo {

    public static final String REDIS_CACHE = "redis_cache";
    public static final String THRIFT_SERVICE = "thrift_service";
    public static final String THRIFT_CSM_SERVICE = "thrift_csm_module";

    /**
     * gearman.
     */
    public static final String GEARMAN_SERVER = "gearman_server";

    public static final String FUNCTION_USER = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_user"), "user");
    
    public static final String FUNCTION_GAME = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_game"), "game");
    
    public static final String FUNCTION_GOLD_LOG = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_gold_log"), "goldLog");
    
    public static final String FUNCTION_GOLD_STATISTIC = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_gold_statistic"), "goldStatistic");

    public static final String FUNCTION_CHEAT_TIENLEN_LOGS = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_cheat_tienlen_logs"), "cheatTienLenLogs");
    
    public static final String FUNCTION_BACCARAT_PAYOUT_CONTROL_LOGS = ConvertUtil.toString(ConfigUtil.getString(GEARMAN_SERVER, "function_baccarat_payout_control_logs"), "baccaratPayoutControlLogs");
    
    /**
     * set config expire.
     */
    public static final int SET_EXPIRES_IN = 1800;

    /**
     * redis generate unique id.
     */
    public static final String AGENT_LOG_ID = "log_id";
}
