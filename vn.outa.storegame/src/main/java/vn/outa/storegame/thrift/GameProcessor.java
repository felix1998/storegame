package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.GameBC;
import vn.outa.storegame.thrift.thrift.game.GameService.Iface;
import vn.outa.storegame.thrift.thrift.game.TGame;
import vn.outa.storegame.thrift.thrift.game.TGameListResult;
import vn.outa.storegame.thrift.thrift.game.TGameResult;

/**
 *
 * @author felix
 */
public class GameProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TGame value) throws TException {
        return GameBC.create(value);
    }

    @Override
    public boolean updateInformation(TGame value) throws TException {
        return GameBC.updateInformation(value);
    }

    @Override
    public boolean updateStatus(TGame value) throws TException {
        return GameBC.updateStatus(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return GameBC.remove(id);
    }

    @Override
    public boolean checkExists(String gameName) throws TException {
        return GameBC.checkExists(gameName);
    }

    @Override
    public TGameResult getById(long id) throws TException {
        return GameBC.getById(id);
    }
    
    @Override
    public TGameResult getByName(String keyword) throws TException {
        return GameBC.getByName(keyword);
    }

    @Override
    public TGameListResult getList(int offset, int count) throws TException {
        return  GameBC.getList(offset, count);
    }
    
    @Override
    public TGameListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  GameBC.getMulti(whereClause, offset, count);
    }
    
    @Override
    public TGameListResult getListDisable(String whereClause, int offset, int count) throws TException {
        return  GameBC.getListDisable(whereClause, offset, count);
    }

}
