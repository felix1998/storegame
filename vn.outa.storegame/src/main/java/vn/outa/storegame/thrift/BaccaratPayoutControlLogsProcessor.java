package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.BaccaratPayoutControlLogsBC;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.BaccaratPayoutControlLogsService.Iface;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogs;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsListResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsResult;

/**
 *
 * @author felix
 */
public class BaccaratPayoutControlLogsProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TBaccaratPayoutControlLogs value) throws TException {
        return BaccaratPayoutControlLogsBC.create(value);
    }

    @Override
    public boolean update(TBaccaratPayoutControlLogs value) throws TException {
        return BaccaratPayoutControlLogsBC.update(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return BaccaratPayoutControlLogsBC.remove(id);
    }

    @Override
    public TBaccaratPayoutControlLogsResult getById(long id) throws TException {
        return BaccaratPayoutControlLogsBC.getById(id);
    }
    
    @Override
    public TBaccaratPayoutControlLogsListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  BaccaratPayoutControlLogsBC.getMulti(whereClause, offset, count);
    }
    
}
