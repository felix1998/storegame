package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.CheatTienLenLogsBC;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.CheatTienLenLogsService.Iface;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogs;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsListResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsResult;

/**
 *
 * @author felix
 */
public class CheatTienLenLogsProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TCheatTienLenLogs value) throws TException {
        return CheatTienLenLogsBC.create(value);
    }

    @Override
    public boolean update(TCheatTienLenLogs value) throws TException {
        return CheatTienLenLogsBC.update(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return CheatTienLenLogsBC.remove(id);
    }

    @Override
    public TCheatTienLenLogsResult getById(long id) throws TException {
        return CheatTienLenLogsBC.getById(id);
    }
    
    @Override
    public TCheatTienLenLogsListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  CheatTienLenLogsBC.getMulti(whereClause, offset, count);
    }
    
}
