package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.GoldStatisticBC;
import vn.outa.storegame.thrift.thrift.goldstatistic.GoldStatisticService.Iface;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticListResult;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticResult;

/**
 *
 * @author felix
 */
public class GoldStatisticProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TGoldStatistic value) throws TException {
        return GoldStatisticBC.create(value);
    }

    @Override
    public boolean updateGold(TGoldStatistic value) throws TException {
        return GoldStatisticBC.updateGold(value);
    }

    @Override
    public boolean updateStatus(TGoldStatistic value) throws TException {
        return GoldStatisticBC.updateStatus(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return GoldStatisticBC.remove(id);
    }

    @Override
    public TGoldStatisticResult getById(long id) throws TException {
        return GoldStatisticBC.getById(id);
    }
    
    @Override
    public TGoldStatisticListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  GoldStatisticBC.getMulti(whereClause, offset, count);
    }
    
    @Override
    public TGoldStatisticListResult getListDisable(String whereClause, int offset, int count) throws TException {
        return  GoldStatisticBC.getListDisable(whereClause, offset, count);
    }
    
    @Override
    public TGoldStatisticListResult getListByUser(String userName, int offset, int count) {
        return GoldStatisticBC.getListByUser(userName, offset, count);
    }
   
    @Override
    public TGoldStatisticListResult getListByUserGame(String userName, String gameName, int offset, int count) {
        return GoldStatisticBC.getListByUserGame(userName, gameName, offset, count);
    }

    @Override
    public TGoldStatisticListResult getListByUserDay(String userName, long day, int offset, int count) {
        return GoldStatisticBC.getListByUserDay(userName, day, offset, count);
    }
    
}
