package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.UserBC;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thrift.user.TUserListResult;
import vn.outa.storegame.thrift.thrift.user.TUserResult;
import vn.outa.storegame.thrift.thrift.user.UserService.Iface;

/**
 *
 * @author felix
 */
public class UserProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TUser value) throws TException {
        return UserBC.create(value);
    }

    @Override
    public boolean updateInformation(TUser value) throws TException {
        return UserBC.update(value);
    }

    @Override
    public boolean updateMoney(TUser value) throws TException {
        return UserBC.updateMoney(value);
    }

    @Override
    public boolean updateGold(TUser value) throws TException {
        return UserBC.updateGold(value);
    }

    @Override
    public boolean updateStatus(TUser value) throws TException {
        return UserBC.updateStatus(value);
    }

    @Override
    public boolean updateActive(TUser value) throws TException {
        return UserBC.updateActive(value);
    }

    @Override
    public boolean updatePasswrod(String userName, String oldPassword, String newPassword) throws TException {
        return UserBC.updatePassword(userName, oldPassword, newPassword);
    }

    @Override
    public boolean remove(long id) throws TException {
        return UserBC.remove(id);
    }

    @Override
    public boolean checkExists(String userName, String phoneNumber) throws TException {
        return UserBC.checkExists(userName, phoneNumber);
    }

    @Override
    public int login(String userName, String password) throws TException {
        return UserBC.login(userName, password);
    }
    
    @Override
    public boolean logout(String userName) throws TException {
        return UserBC.logout(userName);
    }

    @Override
    public TUserResult getById(long id) throws TException {
        return UserBC.getById(id);
    }
    
    @Override
    public TUserResult getByUserOrPhone(String keyword) throws TException {
        return UserBC.getByUserOrPhone(keyword);
    }
    
    @Override
    public TUserListResult getList(int offset, int count) throws TException {
        return  UserBC.getList(offset, count);
    }
    
    @Override
    public TUserListResult getTopGold(int offset, int count) throws TException {
        return  UserBC.getTopGold(offset, count);
    }
    
    @Override
    public TUserListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  UserBC.getMulti(whereClause, offset, count);
    }
    
    @Override
    public TUserListResult getListDisable(String whereClause, int offset, int count) throws TException {
        return  UserBC.getListDisable(whereClause, offset, count);
    }
    
    @Override
    public TUserListResult getListDelete(String whereClause, int offset, int count) throws TException {
        return  UserBC.getListDelete(whereClause, offset, count);
    }

}
