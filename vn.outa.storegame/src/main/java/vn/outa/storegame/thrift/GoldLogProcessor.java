package vn.outa.storegame.thrift;

import org.apache.thrift.TException;
import vn.outa.storegame.business.GoldLogBC;
import vn.outa.storegame.thrift.thrift.goldLog.GoldLogService.Iface;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLog;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogListResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogResult;

/**
 *
 * @author felix
 */
public class GoldLogProcessor implements Iface{
    
     @Override
    public boolean ping() throws TException {
        System.out.println("ping");
        return true;
    }

    @Override
    public boolean create(TGoldLog value) throws TException {
        return GoldLogBC.create(value);
    }

    @Override
    public boolean updateGold(TGoldLog value) throws TException {
        return GoldLogBC.updateGold(value);
    }

    @Override
    public boolean updateStatus(TGoldLog value) throws TException {
        return GoldLogBC.updateStatus(value);
    }

    @Override
    public boolean remove(long id) throws TException {
        return GoldLogBC.remove(id);
    }

    @Override
    public TGoldLogResult getById(long id) throws TException {
        return GoldLogBC.getById(id);
    }
    
    @Override
    public TGoldLogListResult getMulti(String whereClause, int offset, int count) throws TException {
        return  GoldLogBC.getMulti(whereClause, offset, count);
    }
    
    @Override
    public TGoldLogListResult getListDisable(String whereClause, int offset, int count) throws TException {
        return  GoldLogBC.getListDisable(whereClause, offset, count);
    }

    @Override
    public TGoldLogListResult getListByUser(String userName, int offset, int count) throws TException {
        return GoldLogBC.getListByUser(userName, offset, count);
    }
   
    @Override
    public TGoldLogListResult getListByUserGame(String userName, String gameName, int offset, int count) throws TException {
        return GoldLogBC.getListByUserGame(userName, gameName, offset, count);
    }
    
    @Override
    public TGoldLogListResult getListByUserDay(String userName, long day, int offset, int count) throws TException {
        return GoldLogBC.getListByUserDay(userName, day, offset, count);
    }
    
    @Override
    public TGoldLogListResult getListByUserGameDay(String userName, String gameName, long day, int offset, int count) throws TException {
        return GoldLogBC.getListByUserGameDay(userName, gameName, day, offset, count);
    }

}
