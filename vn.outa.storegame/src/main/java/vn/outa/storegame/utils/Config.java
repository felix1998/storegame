package vn.outa.storegame.utils;

import java.util.Iterator;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import java.io.File;
import java.util.HashMap;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.log4j.Logger;
import java.util.Map;

public class Config
{
    private static final Map<String, String> localconfig;
    public static final String CONFIG_HOME = "conf";
    public static final String CONFIG_FILE = "config.ini";
    private static final Logger logger;
    static final CompositeConfiguration config;
    
    public static String getParam(final String section, final String name) {
        final String key = section + "." + name;
        String value = Config.localconfig.get(key);
        if (value != null) {
            return value;
        }
        value = Config.config.getString(key);
        if (value != null) {
            Config.localconfig.put(key, value);
        }
        return value;
    }
    
    static {
        localconfig = new HashMap<String, String>();
        logger = Logger.getLogger((Class)Config.class);
        String HOME_PATH = System.getProperty("apppath");
        String APP_ENV = System.getProperty("appenv");
        if (APP_ENV == null) {
            APP_ENV = "";
        }
        if (!"".equals(APP_ENV)) {
            APP_ENV += ".";
        }
        if (HOME_PATH == null) {
            HOME_PATH = "";
        }
        else {
            HOME_PATH += File.separator;
        }
        config = new CompositeConfiguration();
        final File configFile = new File(HOME_PATH + "conf" + File.separator + APP_ENV + "config.ini");
        try {
            Config.config.addConfiguration((Configuration)new HierarchicalINIConfiguration(configFile));
            final Iterator ii = Config.config.getKeys();
            while (ii.hasNext()) {
                final String key = (String) ii.next();
                Config.localconfig.put(key, Config.config.getString(key));
            }
        }
        catch (ConfigurationException e) {
            Config.logger.error((Object)e.getMessage());
            System.exit(1);
        }
    }
}
