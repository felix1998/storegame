package vn.outa.storegame.utils;

import java.util.Date;

/**
 *
 * @author felix
 */
public class ConvertDate {
    
    
    /**
     * convert long day to string long day
     * @param date - long date
     * @return string date format dd/mm/yyyy
     */
    public static String getLongDate(final Long date) {
        String result = "";
        if(date == null){
            return result;
        }
        Date value = new Date(date);
        result = value.toString();
        return result;
    }
    
    /**
     * convert long day to string short day
     * @param date - long date
     * @return string date format dd/mm/yyyy
     */
    public static String getShortDate(final Long date) {
        String result = "";
        if(date == null){
            return result;
        }
        Date value = new Date(date);
        result = value.getDate() + "/" + (value.getMonth()+1) + "/" + (value.getYear()+1900);
        return result;
    }
}
