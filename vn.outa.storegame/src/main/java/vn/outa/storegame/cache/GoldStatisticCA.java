package vn.outa.storegame.cache;

import redis.clients.jedis.Jedis;
import vn.outa.framework.util.JsonUtil;
import vn.outa.storegame.helper.CacheKey;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;
import zlib.db.RedisDBClient;

/**
 *
 * @author felix
 */
public class GoldStatisticCA {
    
    public static boolean setGoldStatistic(TGoldStatistic value) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.GOLD_STATISTIC_KEY, value.getId());
            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static TGoldStatistic getGoldStatistic(long goldStatisticId) {
        TGoldStatistic result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.GOLD_STATISTIC_KEY, goldStatisticId);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TGoldStatistic.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
  
    public static TGoldStatistic getGoldStatistic(String userName, String gameName, String day) {
        TGoldStatistic result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String member = String.format(userName, "_", gameName, "_", day);
            String keyID = redis.hget(CacheKey.GOLD_STATISTIC_LIST_SYN_KEY, member);
            if(keyID == null || keyID.length() == 0){
                return result;
            }
            
            String key = String.format(CacheKey.GOLD_STATISTIC_KEY, keyID);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TGoldStatistic.class);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static boolean removeGoldStatistic(TGoldStatistic value) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            String key = String.format(CacheKey.GOLD_STATISTIC_KEY, value.getId());
            result = redis.del(key) >= 0;
            
            key = String.format(value.getUserName(), "_", value.getGameName(), "_", value.getDay());
            redis.hdel(CacheKey.GOLD_STATISTIC_LIST_SYN_KEY, key);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }
      
    public static boolean addGoldStatistic(String userName, String gameName, String day, long goldStatisticId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String member = String.format(userName, "_", gameName, "_", day);
            result = redis.hset(CacheKey.GOLD_STATISTIC_LIST_SYN_KEY, member, String.valueOf(goldStatisticId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.set(CacheKey.GOLD_STATISTIC_LASTID_KEY, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static long getLastID() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String data =  redis.get(CacheKey.GOLD_STATISTIC_LASTID_KEY);
            if(data == null || data.length() == 0){
                return result;
            }
            result = Long.valueOf(data);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
}
