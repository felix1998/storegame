package vn.outa.storegame.cache;

import redis.clients.jedis.Jedis;
import vn.outa.framework.util.JsonUtil;
import vn.outa.storegame.helper.CacheKey;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogs;
import zlib.db.RedisDBClient;

/**
 *
 * @author felix
 */
public class CheatTienLenLogsCA {
    
    public static boolean setCheatTienLenLogs(TCheatTienLenLogs value) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.CHEAT_TIENLEN_LOGS_KEY, value.getId());
            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static TCheatTienLenLogs getCheatTienLenLogs(long id) {
        TCheatTienLenLogs result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.CHEAT_TIENLEN_LOGS_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TCheatTienLenLogs.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static boolean removeCheatTienLenLogs(TCheatTienLenLogs value) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            String key = String.format(CacheKey.CHEAT_TIENLEN_LOGS_KEY, value.getId());
            result = redis.del(key) >= 0;
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }
    
    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.set(CacheKey.CHEAT_TIENLEN_LOGS_LASTID_KEY, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static long getLastID() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String data =  redis.get(CacheKey.CHEAT_TIENLEN_LOGS_LASTID_KEY);
            if(data == null || data.length() == 0){
                return result;
            }
            result = Long.valueOf(data);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
}
