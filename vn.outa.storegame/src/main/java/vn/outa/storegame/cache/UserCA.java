package vn.outa.storegame.cache;

import java.util.ArrayList;
import java.util.List;
import redis.clients.jedis.Jedis;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import vn.outa.storegame.helper.CacheKey;
import vn.outa.storegame.thrift.thrift.user.TUser;
import zlib.db.RedisDBClient;

/**
 *
 * @author felix
 */
public class UserCA {
    
    public static boolean setUser(TUser value, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.USER_KEY, value.getId());
            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
            
            if (result && isAddList) {
                if(isList()){
                    result = addList(value.getId(), value.getId());
                }
                if(isListGold()){
                    result = addListGold(value.getGold(), value.getId());
                }
                if(isListMoney()){
                    result = addListMoney(value.getMoney(), value.getId());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static TUser getUser(long id) {
        TUser result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.USER_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TUser.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
     
    public static boolean addUser(String userName, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.hset(CacheKey.USER_LIST_NAME_KEY, userName, String.valueOf(eventId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static TUser getUser(String userName) {
        TUser result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String id = redis.hget(CacheKey.USER_LIST_NAME_KEY, userName);
            if(id == null || id.length() == 0){
                return result;
            }

            String key = String.format(CacheKey.USER_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TUser.class);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static boolean removeUser(TUser value) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            String key = String.format(CacheKey.USER_KEY, value.getId());
            result = redis.del(key) > 0;
            
            /* REMOVE IN ALL LIST */
            redis.zrem(CacheKey.USER_LIST_KEY, String.valueOf(value.getId()));
            redis.zrem(CacheKey.USER_LIST_GOLD_KEY, String.valueOf(value.getId()));
            redis.zrem(CacheKey.USER_LIST_MONEY_KEY, String.valueOf(value.getId()));
            redis.hdel(CacheKey.USER_LIST_NAME_KEY, value.getUserName());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }
    
    public static boolean addList(double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zadd(CacheKey.USER_LIST_KEY, score, ConvertUtil.toString(eventId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static List<Long> getList(int offset, int count) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(CacheKey.USER_LIST_KEY, offset, count);
            for(String id : list){
                result.add(Long.valueOf(id));
            }
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean addListGold(double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zadd(CacheKey.USER_LIST_GOLD_KEY, score, ConvertUtil.toString(eventId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static List<Long> getListGold(int offset, int count) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(CacheKey.USER_LIST_GOLD_KEY, offset, count);
            for(String id : list){
                result.add(Long.valueOf(id));
            }
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean addListMoney(double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zadd(CacheKey.USER_LIST_MONEY_KEY, score, ConvertUtil.toString(eventId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static List<Long> getListMoney(int offset, int count) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(CacheKey.USER_LIST_MONEY_KEY, offset, count);
            for(String id : list){
                result.add(Long.valueOf(id));
            }
            
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static void removeItemInList(TUser value) {
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            redis.zrem(CacheKey.USER_LIST_KEY, String.valueOf(value.getId()));
            redis.zrem(CacheKey.USER_LIST_GOLD_KEY, String.valueOf(value.getId()));
            redis.zrem(CacheKey.USER_LIST_MONEY_KEY, String.valueOf(value.getId()));
            redis.hdel(CacheKey.USER_LIST_NAME_KEY, value.getUserName());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
    }

    public static boolean isList() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(CacheKey.USER_LIST_KEY);
                
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean isListGold() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(CacheKey.USER_LIST_GOLD_KEY);
                
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean isListMoney() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(CacheKey.USER_LIST_MONEY_KEY);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.set(CacheKey.USER_LASTID_KEY, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static long getLastID() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String data =  redis.get(CacheKey.USER_LASTID_KEY);
            if(data == null || data.length() == 0){
                return result;
            }
            
            result = Long.valueOf(data);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
}
