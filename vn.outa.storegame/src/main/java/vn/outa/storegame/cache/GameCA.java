package vn.outa.storegame.cache;

import java.util.ArrayList;
import java.util.List;
import redis.clients.jedis.Jedis;
import vn.outa.framework.util.ConvertUtil;
import vn.outa.framework.util.JsonUtil;
import vn.outa.storegame.helper.CacheKey;
import vn.outa.storegame.thrift.thrift.game.TGame;
import zlib.db.RedisDBClient;

/**
 *
 * @author felix
 */
public class GameCA {
    
    public static boolean setGame(TGame value, boolean isAddList) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {

            String key = String.format(CacheKey.GAME_KEY, value.getId());
            result = redis.set(key, JsonUtil.serialize(value)).equalsIgnoreCase("OK");
            if (result && isAddList) {
                if(isListKey()){
                    result = addList(value.getId(), value.getId());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static TGame getGame(long id) {
        TGame result = null;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String key = String.format(CacheKey.GAME_KEY, id);
            String data = redis.get(key);
            if (data == null || data.length() == 0) {
                return result;
            }
            
            result =JsonUtil.deserialize(data, TGame.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static boolean removeGame(long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            String key = String.format(CacheKey.GAME_KEY, eventId);
            result = redis.del(key) > 0;
            
            /* REMOVE IN LIST */
            redis.zrem(CacheKey.GAME_LIST_KEY, String.valueOf(eventId));
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }
    
    public static boolean removeItemInList(long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();
        
        try{
            result = redis.zrem(CacheKey.GAME_LIST_KEY, String.valueOf(eventId)) > 0;
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        
        return result;
    }
    
    public static boolean addList(double score, long eventId) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.zadd(CacheKey.GAME_LIST_KEY, score, ConvertUtil.toString(eventId)) > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static List<Long> getList(int recordStart, int pageSize) {
        List<Long> result = new ArrayList<>();
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            List<String> list = redis.zrevrange(CacheKey.GAME_LIST_KEY, recordStart, pageSize);
            for(String id : list){
                result.add(Long.valueOf(id));
            }
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean isListKey() {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.exists(CacheKey.GAME_LIST_KEY);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }
        return result;
    }
    
    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.set(CacheKey.GAME_LASTID_KEY, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static long getLastID() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String data =  redis.get(CacheKey.GAME_LASTID_KEY);
            if(data == null || data.length() == 0){
                return result;
            }
            
            result = Long.valueOf(data);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
}
