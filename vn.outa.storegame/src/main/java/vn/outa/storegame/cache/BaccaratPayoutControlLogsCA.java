package vn.outa.storegame.cache;

import redis.clients.jedis.Jedis;
import vn.outa.storegame.helper.CacheKey;
import zlib.db.RedisDBClient;

/**
 *
 * @author felix
 */
public class BaccaratPayoutControlLogsCA {
    
    public static boolean setLastID(long id) {
        boolean result = false;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            result = redis.set(CacheKey.BACCARAT_PAYOUT_CONTROL_LOGS_LASTID_KEY, String.valueOf(id)).equalsIgnoreCase("OK");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
    public static long getLastID() {
        long result = 0;
        Jedis redis = RedisDBClient.instance().getJedis();

        try {
            String data =  redis.get(CacheKey.BACCARAT_PAYOUT_CONTROL_LOGS_LASTID_KEY);
            if(data == null || data.length() == 0){
                return result;
            }
            result = Long.valueOf(data);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        } finally {
            redis.close();
        }

        return result;
    }
    
}
