include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.game

enum EJobType {
    CREATE,
    UPDATE_INFOR,
    UPDATE_STATUS,
    DELETE,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
    DELETE = 3,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1,
}

struct TGame {
    1: i64 id,
    2: string gameName,
    3: string displayName,
    4: string decription,
    5: i32 status,
    6: string startDate,
    7: string endDate,
}

struct TGameResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGame value,
}

struct TGameListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGame> listData,
    3: i64 totalRecord,
}

service GameService extends TServiceBases.ServiceBases {
    bool create(1: TGame value),
    bool updateInformation(1: TGame value),
    bool updateStatus(1: TGame value),
    bool remove(1: i64 id),
    bool checkExists(1:string value),
    TGameResult getById(1: i64 id),
    TGameResult getByName(1: string keyword),
    TGameListResult getList(1: i32 offset, 2: i32 count),
    TGameListResult getMulti(1: string whereClause, 2: i32 offset, 3: i32 count),
    TGameListResult getListDisable(1: string whereClause, 2: i32 offset, 3: i32 count),
}
