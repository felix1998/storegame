namespace java vn.outa.storegame.thrift.thrift.tcommon

enum ServiceStatus {
    STARTED = 0,
    STOPPED = 1,
    SHUTTING_DOWN = 2
}

enum PlayerStatus {
    ONLINE = 1,
    OFFLINE = 2,
    INACTIVE = 3
}

struct TResponseInfo {
    1: list<string> playerIdReceive,
    2: binary messageData
}

struct OpAuth {
    1: string auth_key,
    2: string source
}

struct TRequest {
    1: string playerId,
    2: i32 msgType,
    3: binary msgData
}

service ServiceBases {
    bool ping()
}
