include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.goldstatistic

enum EJobType {
    CREATE,
    UPDATE_GOLD,
    UPDATE_STATUS,
    DELETE,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1,
}

struct TGoldStatistic {
    1: i64 id,
    2: string userName,
    3: string gameName,
    4: double totalGold,
    5: string day,
    6: i32 status,
}

struct TGoldStatisticResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGoldStatistic value,
}

struct TGoldStatisticListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGoldStatistic> listData,
    3: i64 totalRecord,
}

service GoldStatisticService extends TServiceBases.ServiceBases {
    bool create(1: TGoldStatistic value),
    bool updateGold(1:TGoldStatistic value),
    bool updateStatus(1:TGoldStatistic value),
    bool remove(1: i64 id),
    TGoldStatisticResult getById(1: i64 id),
    TGoldStatisticListResult getMulti(1: string whereClause, 2: i32 offset, 3: i32 count),
    TGoldStatisticListResult getListDisable(1: string whereClause, 2: i32 offset, 3: i32 count),
    TGoldStatisticListResult getListByUser(1:string userName, 2: i32 offset, 3: i32 count),
    TGoldStatisticListResult getListByUserGame(1:string userName, 2:string gameName, 3: i32 offset, 4: i32 count),
    TGoldStatisticListResult getListByUserDay(1:string userName, 2:i64 day, 3: i32 offset, 4: i32 count),
}

