include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.user

enum EJobType {
    CREATE,
    UPDATE,
    UPDATE_MONEY,
    UPDATE_GOLD,
    UPDATE_PASSWORD,
    UPDATE_ACTIVE,
    UPDATE_STATUS,
    DELETE,
    LOGIN,
    LOGOUT,
    CHECK_EXISTS,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
    DELETE = 3,
}

enum EActive {
    ONLINE = 1,
    OFFLINE = 0,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1,
}

struct TUser {
    1: i64 id,
    2: string userName,
    3: string password,
    4: string displayName,
    5: string phoneNumber,
    6: double money,
    7: double gold,
    8: i32 level,
    9: double experence,
    10: i32 win,
    11: i32 lose,
    12: i32 quit,
    13: i32 active
    14: string lastLogin,
    15: i32 gender,
    16: string birthDay,
    17: string email,
    18: string address,
    19: i32 status,
    20: string regisDate,
}

struct TUserResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TUser value
}

struct TUserListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TUser> listData,
    3: i64 totalRecord
}

service UserService extends TServiceBases.ServiceBases {
    bool create(1:TUser value),
    bool updateInformation(1:TUser value),
    bool updateMoney(1:TUser value),
    bool updateGold(1:TUser value),
    bool updateStatus(1:TUser value),
    bool updateActive(1:TUser value),
    bool updatePasswrod(1:string userName, 2: string oldPassword, 3: string newPassword),
    bool remove(1:i64 id),
    bool checkExists(1:string userName, 2:string phoneNumber),
    i32 login(1:string userName, 2: string password),
    bool logout(1:string userName),
    TUserResult getById(1:i64 id),
    TUserResult getByUserOrPhone(1: string keyword),
    TUserListResult getList(1:i32 offset, 2:i32 count),
    TUserListResult getTopGold(1:i32 offset, 2:i32 count),
    TUserListResult getMulti(1:string whereClause, 2:i32 offset, 3:i32 count),
    TUserListResult getListDisable(1:string whereClause, 2:i32 offset, 3:i32 count),
    TUserListResult getListDelete(1:string whereClause, 2:i32 offset, 3:i32 count),
}
