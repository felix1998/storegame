include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.goldLog

enum EJobType {
    CREATE,
    UPDATE_GOLD,
    UPDATE_STATUS,
    DELETE,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}

struct TGoldLog {
    1: i64 id,
    2: string userName,
    3: string gameName,
    4: double goldChange,
    5: i64 date,
    6: i32 status,
}

struct TGoldLogResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TGoldLog value,
}

struct TGoldLogListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TGoldLog> listData,
    3: i64 totalRecord,
}

service GoldLogService extends TServiceBases.ServiceBases {
    bool create(1:TGoldLog value),
    bool updateGold(1:TGoldLog value),
    bool updateStatus(1:TGoldLog value),
    bool remove(1:i64 id),
    TGoldLogResult getById(1:i64 id),
    TGoldLogListResult getMulti(1:string whereClause, 2:i32 offset, 3:i32 count),
    TGoldLogListResult getListDisable(1:string whereClause, 2:i32 offset, 3:i32 count),
    TGoldLogListResult getListByUser(1:string userName, 2:i32 offset, 3:i32 count),
    TGoldLogListResult getListByUserDay(1:string userName, 2:i64 day, 3:i32 offset, 4:i32 count),
    TGoldLogListResult getListByUserGame(1:string userName, 2:string gameName, 3:i32 offset, 4:i32 count),
    TGoldLogListResult getListByUserGameDay(1:string userName, 2:string gameName, 3:i64 day, 4:i32 offset, 5:i32 count),
}