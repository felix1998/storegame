include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.connectorservice

service TConnectorService extends TServiceBases.ServiceBases {
    void pushResponse(1:string token, 2:TServiceBases.TResponseInfo response)
    void pushResponses(1:string token, 2:list<TServiceBases.TResponseInfo> lstResponse)
    void removeSessions(1:string token, 2:list<string> lstSessionId)
    void removeSession(1:string token, 2:string sessionId)
    list<TServiceBases.TResponseInfo> perform(1:string token, 2:list<i32> moduleId, 3:TServiceBases.TRequest msgData)
    list<TServiceBases.ServiceStatus> getStatus(1:list<i32> moduleId)
    TServiceBases.PlayerStatus getPlayerStatus(1:string token, 2:string palyerId)
    bool updateMoney(1:string token, 2:string playerId, 3:i64 money, 4:i64 totalMoney)
}
