include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs

enum EJobType {
    CREATE,
    UPDATE,
    DELETE,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}

struct TBaccaratPayoutControlLogs {
    1: i64 id,
    2: i32 gameId,
    3: i64 roundId,
    4: double rtpConfig,
    5: double rtpStart,
    6: i64 totalPayInStart,
    7: i64 totalPayOutStart,
    8: double rtpEnd,
    9: i64 totalPayInEnd,
    10: i64 totalPayOutEnd,
    11: i64 roundPayIn,
    12: i64 roundPayOut,
    13: i64 payInPlayerPair,
    14: i64 payInPlayer,
    15: i64 payInBanker,
    16: i64 payInTie,
    17: i64 payInBankerPair,
    18: i64 payOutPlayerPair,
    19: i64 payOutPlayer,
    20: i64 payOutBanker,
    21: i64 payOutTie,
    22: i64 payOutBankerPair,
    23: string originalResult,
    24: string randomResults,
    25: string result,
    26: i32 isActive,
    27: i64 time,
}

struct TBaccaratPayoutControlLogsResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TBaccaratPayoutControlLogs value,
}

struct TBaccaratPayoutControlLogsListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TBaccaratPayoutControlLogs> listData,
    3: i64 totalRecord,
}

service BaccaratPayoutControlLogsService extends TServiceBases.ServiceBases {
    bool create(1:TBaccaratPayoutControlLogs value),
    bool update(1:TBaccaratPayoutControlLogs value),
    bool remove(1:i64 id),
    TBaccaratPayoutControlLogsResult getById(1:i64 id),
    TBaccaratPayoutControlLogsListResult getMulti(1:string whereClause, 2:i32 offset, 3:i32 count),
}
