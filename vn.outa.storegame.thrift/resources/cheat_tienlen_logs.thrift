include "TServiceBases.thrift"

namespace java vn.outa.storegame.thrift.thrift.cheatTienLenLogs

enum EJobType {
    CREATE,
    UPDATE,
    DELETE,
}

enum EStatus {
    ENABLE = 1,
    DISABLE = 2,
}

enum EStatusResult {
    OK = 0,
    FAIL = 1
}

struct TCheatTienLenLogs {
    1: i64 id,
    2: double minMoneyBot,
    3: double minMoneyBotAntrang,
    4: double minTotalMoneyBot,
    5: string gameConfig,
    6: string bots,
    7: i64 roundId,
    8: string cheatBotId,
    9: string cheatMode,
    10: double winLost,
    11: string endRoundGameConfig,
    12: double totalWinLostBefore,
    13: double totalWinLostAfter,
    14: i64 time,
}

struct TCheatTienLenLogsResult {
    1: EStatusResult status = EStatusResult.OK,
    2: TCheatTienLenLogs value,
}

struct TCheatTienLenLogsListResult {
    1: EStatusResult status = EStatusResult.OK,
    2: list<TCheatTienLenLogs> listData,
    3: i64 totalRecord,
}

service CheatTienLenLogsService extends TServiceBases.ServiceBases {
    bool create(1:TCheatTienLenLogs value),
    bool update(1:TCheatTienLenLogs value),
    bool remove(1:i64 id),
    TCheatTienLenLogsResult getById(1:i64 id),
    TCheatTienLenLogsListResult getMulti(1:string whereClause, 2:i32 offset, 3:i32 count),
}
