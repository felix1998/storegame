/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package vn.outa.storegame.thrift.thrift.user;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum EActive implements org.apache.thrift.TEnum {
  ONLINE(1),
  OFFLINE(0);

  private final int value;

  private EActive(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static EActive findByValue(int value) { 
    switch (value) {
      case 1:
        return ONLINE;
      case 0:
        return OFFLINE;
      default:
        return null;
    }
  }
}
