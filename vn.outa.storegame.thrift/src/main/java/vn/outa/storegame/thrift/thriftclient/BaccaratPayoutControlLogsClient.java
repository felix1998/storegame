package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.BaccaratPayoutControlLogsService;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.BaccaratPayoutControlLogsService.Client;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogs;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsListResult;
import vn.outa.storegame.thrift.thrift.baccaratPayoutControlLogs.TBaccaratPayoutControlLogsResult;



/**
 *
 * @author felix
 */
public class BaccaratPayoutControlLogsClient implements BaccaratPayoutControlLogsService.Iface{

    private static final Logger logger = Logger.getLogger(BaccaratPayoutControlLogsClient.class);
    private final TClientManager clientManager;
    
    public BaccaratPayoutControlLogsClient (String section){
        clientManager = TClientManager.getInstance(section, BaccaratPayoutControlLogsService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TBaccaratPayoutControlLogs value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean update(TBaccaratPayoutControlLogs value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.update(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TBaccaratPayoutControlLogsResult getById(long id) {
        TBaccaratPayoutControlLogsResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TBaccaratPayoutControlLogsResult(EStatusResult.FAIL, null);
        }
        return result;
    }
    
    @Override
    public TBaccaratPayoutControlLogsListResult getMulti(String whereClause, int offset, int count) {
        TBaccaratPayoutControlLogsListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TBaccaratPayoutControlLogsListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
