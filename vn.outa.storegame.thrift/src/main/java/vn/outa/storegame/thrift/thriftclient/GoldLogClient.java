package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.goldLog.EStatusResult;
import vn.outa.storegame.thrift.thrift.goldLog.GoldLogService;
import vn.outa.storegame.thrift.thrift.goldLog.GoldLogService.Client;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLog;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogListResult;
import vn.outa.storegame.thrift.thrift.goldLog.TGoldLogResult;



/**
 *
 * @author felix
 */
public class GoldLogClient implements GoldLogService.Iface{

    private static final Logger logger = Logger.getLogger(GoldLogClient.class);
    private final TClientManager clientManager;
    
    public GoldLogClient (String section){
        clientManager = TClientManager.getInstance(section, GoldLogService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TGoldLog value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateGold(TGoldLog value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateGold(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean updateStatus(TGoldLog value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateStatus(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update status error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TGoldLogResult getById(long id) {
        TGoldLogResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogResult(EStatusResult.FAIL, null);
        }
        return result;
    }
    
    @Override
    public TGoldLogListResult getMulti(String whereClause, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldLogListResult getListDisable(String whereClause, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListDisable(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list disable by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

    @Override
    public TGoldLogListResult getListByUser(String userName, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUser(userName, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by user error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldLogListResult getListByUserGame(String userName, String gameName, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUserGame(userName, gameName, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by user game error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldLogListResult getListByUserDay(String userName, long day, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUserDay(userName, day, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by user day error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldLogListResult getListByUserGameDay(String userName, String gameName, long day, int offset, int count) {
        TGoldLogListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUserGameDay(userName, gameName, day, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by user game day error", ex);
            clientManager.invalidate(tco);
            result = new TGoldLogListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
