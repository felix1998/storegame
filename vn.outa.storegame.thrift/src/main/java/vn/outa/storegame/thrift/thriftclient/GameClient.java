package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.game.GameService;
import vn.outa.storegame.thrift.thrift.game.GameService.Client;
import vn.outa.storegame.thrift.thrift.game.EStatusResult;
import vn.outa.storegame.thrift.thrift.game.TGame;
import vn.outa.storegame.thrift.thrift.game.TGameListResult;
import vn.outa.storegame.thrift.thrift.game.TGameResult;



/**
 *
 * @author felix
 */
public class GameClient implements GameService.Iface{

    private static final Logger logger = Logger.getLogger(GameClient.class);
    private final TClientManager clientManager;
    
    public GameClient (String section){
        clientManager = TClientManager.getInstance(section, GameService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TGame value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateInformation(TGame value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateInformation(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update information error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean updateStatus(TGame value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateStatus(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update status error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean checkExists(String value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.checkExists(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("check exists error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TGameResult getById(long id) {
        TGameResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TGameResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TGameResult getByName(String keywork) {
        TGameResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getByName(keywork);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by name error", ex);
            clientManager.invalidate(tco);
            result = new TGameResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TGameListResult getList(int offset, int count) {
        TGameListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getList(offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list error", ex);
            clientManager.invalidate(tco);
            result = new TGameListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGameListResult getMulti(String whereClause, int offset, int count) {
        TGameListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGameListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGameListResult getListDisable(String whereClause, int offset, int count) {
        TGameListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListDisable(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list disable by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGameListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
