package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.goldstatistic.EStatusResult;
import vn.outa.storegame.thrift.thrift.goldstatistic.GoldStatisticService;
import vn.outa.storegame.thrift.thrift.goldstatistic.GoldStatisticService.Client;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatistic;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticListResult;
import vn.outa.storegame.thrift.thrift.goldstatistic.TGoldStatisticResult;



/**
 *
 * @author felix
 */
public class GoldStatisticClient implements GoldStatisticService.Iface{

    private static final Logger logger = Logger.getLogger(GoldStatisticClient.class);
    private final TClientManager clientManager;
    
    public GoldStatisticClient (String section){
        clientManager = TClientManager.getInstance(section, GoldStatisticService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TGoldStatistic value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateGold(TGoldStatistic value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateGold(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update total gold error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean updateStatus(TGoldStatistic value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateStatus(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update status error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TGoldStatisticResult getById(long id) {
        TGoldStatisticResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TGoldStatisticListResult getMulti(String whereClause, int offset, int count) {
        TGoldStatisticListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldStatisticListResult getListDisable(String whereClause, int offset, int count) {
        TGoldStatisticListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListDisable(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list disable by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldStatisticListResult getListByUser(String userName, int offset, int count) {
        TGoldStatisticListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUser(userName, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list by user error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldStatisticListResult getListByUserGame(String userName, String gameName, int offset, int count) {
        TGoldStatisticListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUserGame(userName, gameName, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list by user game error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TGoldStatisticListResult getListByUserDay(String userName, long day, int offset, int count) {
        TGoldStatisticListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getListByUserDay(userName, day, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list by user day error", ex);
            clientManager.invalidate(tco);
            result = new TGoldStatisticListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
