package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.CheatTienLenLogsService;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.CheatTienLenLogsService.Client;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.EStatusResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogs;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsListResult;
import vn.outa.storegame.thrift.thrift.cheatTienLenLogs.TCheatTienLenLogsResult;



/**
 *
 * @author felix
 */
public class CheatTienLenLogsClient implements CheatTienLenLogsService.Iface{

    private static final Logger logger = Logger.getLogger(CheatTienLenLogsClient.class);
    private final TClientManager clientManager;
    
    public CheatTienLenLogsClient (String section){
        clientManager = TClientManager.getInstance(section, CheatTienLenLogsService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TCheatTienLenLogs value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean update(TCheatTienLenLogs value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.update(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TCheatTienLenLogsResult getById(long id) {
        TCheatTienLenLogsResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TCheatTienLenLogsResult(EStatusResult.FAIL, null);
        }
        return result;
    }
    
    @Override
    public TCheatTienLenLogsListResult getMulti(String whereClause, int offset, int count) {
        TCheatTienLenLogsListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TCheatTienLenLogsListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
