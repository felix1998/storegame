package vn.outa.storegame.thrift.thriftclient;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import vn.outa.framework.thrift.client.TClientManager;
import vn.outa.framework.thrift.client.TClientObject;
import vn.outa.storegame.thrift.thrift.user.EStatusResult;
import vn.outa.storegame.thrift.thrift.user.TUser;
import vn.outa.storegame.thrift.thrift.user.TUserListResult;
import vn.outa.storegame.thrift.thrift.user.TUserResult;
import vn.outa.storegame.thrift.thrift.user.UserService;
import vn.outa.storegame.thrift.thrift.user.UserService.Client;

/**
 *
 * @author felix
 */
public class UserClient implements UserService.Iface{

    private static final Logger logger = Logger.getLogger(UserClient.class);
    private final TClientManager clientManager;
    
    public UserClient (String section){
        clientManager = TClientManager.getInstance(section, UserService.class);
    }
    
    @Override
    public boolean ping() {
         boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.ping();
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("ping error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean create(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.create(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("create error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateInformation(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateInformation(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }
  
    @Override
    public boolean updateMoney(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateMoney(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update money error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateGold(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateGold(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update money error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateStatus(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateStatus(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update status error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updateActive(TUser value) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updateActive(value);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update active error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean updatePasswrod(String userName, String oldPassword, String newPassword) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.updatePasswrod(userName, oldPassword, newPassword);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("update password error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean remove(long id) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.remove(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("remove error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public boolean checkExists(String userName, String phoneNumber) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.checkExists(userName, phoneNumber);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("check user exists error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public int login(String userName, String password) {
        int result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.login(userName, password);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("login error", ex);
            clientManager.invalidate(tco);
            result = 1;
        }
        return result;
    }
    
    @Override
    public boolean logout(String userName) {
        boolean result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.logout(userName);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("logout error", ex);
            clientManager.invalidate(tco);
            result = false;
        }
        return result;
    }

    @Override
    public TUserResult getById(long id) {
        TUserResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getById(id);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by id error", ex);
            clientManager.invalidate(tco);
            result = new TUserResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TUserResult getByUserOrPhone(String keyword) {
        TUserResult result;
        TClientObject<Client> tco = clientManager.borrow();
        
        try {
            result = tco.client.getByUserOrPhone(keyword);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get by username or phonenumber error", ex);
            clientManager.invalidate(tco);
            result = new TUserResult(EStatusResult.FAIL, null);
        }
        return result;
    }

    @Override
    public TUserListResult getList(int offset, int count) {
        TUserListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getList(offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list error", ex);
            clientManager.invalidate(tco);
            result = new TUserListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TUserListResult getTopGold(int offset, int count) {
        TUserListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getTopGold(offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list top gold user error", ex);
            clientManager.invalidate(tco);
            result = new TUserListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TUserListResult getMulti(String whereClause, int offset, int count) {
        TUserListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getMulti(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list multi by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TUserListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    @Override
    public TUserListResult getListDisable(String whereClause, int offset, int count) {
        TUserListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListDisable(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list disable by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TUserListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }
    
    @Override
    public TUserListResult getListDelete(String whereClause, int offset, int count) {
        TUserListResult result;
        TClientObject<Client> tco = clientManager.borrow();
        try {
            result = tco.client.getListDelete(whereClause, offset, count);
            clientManager.giveBack(tco);
            return result;
        } catch (TException ex) {
            logger.error("get list delete by where clause error", ex);
            clientManager.invalidate(tco);
            result = new TUserListResult(EStatusResult.FAIL, null, 0);
        }
        return result;
    }

}
